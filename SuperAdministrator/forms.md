

Forms created by all the account administrators are visible to super administrator. The form info, data submitted by field users for the forms are also accesible to super administrator.

1.	When clicked on Forms tab, Super administrator can see the list of Forms available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU163.jpg"> </p>

<div align="center"> 

**`Fig.159` Forms page**

</div>

2.	Super administrator can search, preview the Form, view submitted records of a form, share link of the form, view form information, import form as template.


### Form Preview

1.	Super administrator can preview the Form.

2.	To Preview the Form, go to the form and hover on the form card, displays options.

3.	Click on preview button a popup will be open with all the widgets in the form.

<p align="center"> <img width="1000" height="450" src="media/SU/SU172.jpg"> </p>

<div align="center"> 

**`Fig.160` Form preview**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU173.jpg"> </p>

<div align="center"> 

**`Fig.161` Form preview**

</div>

#### Form Import as Template

1.	Go to the Form tab and mouse hover on the Form, Import button is visible. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU174.jpg"> </p>

<div align="center"> 

**`Fig.162` Import Form as template**

</div>

2.	On click import icon, import as template popup will opened.

<p align="center"> <img width="1000" height="450" src="media/SU/SU175.jpg"> </p>

<div align="center"> 

**`Fig.163` Import Form as template**

</div>

3.	Fill the following fields:

 -	Template name

 -	Categories

>Categories can be selected multiple

<p align="center"> <img width="1000" height="450" src="media/SU/SU176.jpg"> </p>

<div align="center"> 

**`Fig.165` Import Form as template**

</div>

4. After filling the fields click on "Create" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU177.jpg"> </p>

<div align="center"> 

**`Fig.166` Import Form as template**

</div>

5. On successful import, a toasted message will be shown as "Form imported as template successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU178.jpg"> </p>

<div align="center"> 

**`Fig.167` Form imported message**

</div>

6.	Super administrator can cancel the import process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU179.jpg"> </p>

<div align="center"> 

**`Fig.168` Cancel Import Form as template**

</div>

### Form Submitted Records


* Administrator can export submitted Records
* Export to excel
* Export to Mail
* Export to PDF
* See on map
* Re-assign

To know about more information, click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)


### Form information

1.	Mouse hover on form, application displays 'info' icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU217.jpg"> </p>

<div align="center"> 

**`Fig.169` Form info**

</div>

2.	Super administrator can view the form information by clicking on info icon on the form.

3.	The following information is shown in a table:

 -	Form Name

 -	Created by

 -	Creation date

 -	No of assignments

 -	No of cases collected

 -	Last form modified date

 -	Last downloaded date

 -	Last downloaded by

 -	No of users downloaded


<p align="center"> <img width="1000" height="450" src="media/SU/SU218.jpg"> </p>

<div align="center"> 

**`Fig.170` Form info**

</div>


### Form Search

1.	Super administrator can search the form by providing the input as form name in the search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU163.jpg"> </p>

<div align="center"> 

**`Fig.171` Search Forms**

</div>

2.	Result for the input provided by Super administrator.

<p align="center"> <img width="1000" height="450" src="media/SU/SU164.jpg"> </p>

<div align="center"> 

**`Fig.172` Search Forms**

</div>

### Filter Forms


1. Click on filter icon and select "All", Shows both public and private forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU165.jpg"> </p>

<div align="center"> 

**`Fig.123` Filter Forms**

</div>

2.	Click on filter icon and select Public, shows all the public forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU166.jpg"> </p>

<div align="center"> 

**`Fig.124` Filter Forms**

</div>

3.	Click on filter icon and select Private, shows all the private forms.

<p align="center"> <img width="1000" height="450" src="media/SU/SU167.jpg"> </p>

<div align="center"> 

**`Fig.125` Filter Forms**

</div>

4.	Forms can be categorized by groups  by clicking on Group by category icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU169.jpg"> </p>

<div align="center"> 

**`Fig.126` Group by category of Forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU170.jpg"> </p>

<div align="center"> 

**`Fig.127` Group by category of Forms**

</div>
