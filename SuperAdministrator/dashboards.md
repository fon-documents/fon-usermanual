

The Dashboards provide the statistical information present under the login. Following are the different roles under the application and their respective dashboards.
Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Root

Root has two type of entities:

  1. Usage of System

  2. Account Specific

<p align="center"> <img width="1000" height="450" src="media/SU/SU292.jpg"> </p>

<div align="center"> 

**`Fig.227` Dashboard**

</div>

-	Under "Usage of System", the following statistics available. The grey boxes with ellipse opens a table with records explaining more about the numeric data. For example, "Consumed Licenses" in the below screenshot.

<p align="center"> <img width="1000" height="450" src="media/SU/SU293.jpg"> </p>

<div align="center"> 

**`Fig.228` Dashboard**

</div>

-	Under "Account Specific" entity we get account specific details. The account can be selected from the dropdown on clicking the ellipse.

<p align="center"> <img width="1000" height="450" src="media/SU/SU294.jpg"> </p>

<div align="center"> 

**`Fig.229` Dashboard**

</div>

-	The grey boxes with ellipse provide data in tabular form. Below are the details available under Account Specific entity. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU295.jpg"> </p>

<div align="center"> 

**`Fig.230` Dashboard**

</div>
