﻿


Accounts are created by Super administrator to privatise the data from one account to another account.

Super administrator can create, update, delete, view, search accounts and can disassociate Administrators, Moderator, and Field Users of an Account.

### View list of Accounts available

1.	Super administrator can view all the Accounts

<p align="center"> <img width="1000" height="450" src="media/SU/SU3.jpg"> </p>

<div align="center"> 

**`Fig.3` list of All Accounts**

</div>


### Account creation

1.	In the right-hand top corner, ellipse button in visible in accounts page. 

2.	On Click "ellipse" and then click on "create" button to create new 'Account'.

<p align="center"> <img width="1000" height="450" src="media/SU/SU7.jpg"> </p>

<div align="center"> 

**`Fig.4` create account**

</div>

3.	Account creation form gets opened.

<p align="center"> <img width="1000" height="450" src="media/SU/SU8.jpg"> </p>

<div align="center"> 

**`Fig.5` create form of account**

</div>

4.	Fill the mandatory fields and click the create button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU9.jpg"> </p>

<div align="center"> 

**`Fig.6` Create account**

</div>

5.	After successful creation Super administrator can see a toasted message as "Account created successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/SU10.jpg"> </p>

<div align="center"> 

**`Fig.7` Create account**

</div>

!> Duplicate Account names are not allowed.

6.	In between Super administrator can cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU12.jpg"> </p>

<div align="center"> 

**`Fig.8` Cancel account creation**

</div>

7.	Click "cancel", It shows the alert of data loss.

<p align="center"> <img width="1000" height="450" src="media/SU/SU13.jpg"> </p>

<div align="center"> 

**`Fig.9` Cancel account creation**

</div>

8. Click on "Ok", application redirects to Accounts page.

9. Click on "Cancel", application remains in the same page.

### Account Edit/Update

1.	To edit 'Account', hover on the account, following buttons are visible

 - Edit

 - Disassociation

 - Delete

<p align="center"> <img width="1000" height="450" src="media/SU/SU14.jpg"> </p>

<div align="center"> 

**`Fig.10` Edit account**

</div>

2.	Click on "Edit" button, view the existing data, and Super administrator can update Email and Description fields.

<p align="center"> <img width="1000" height="450" src="media/SU/SU15.jpg"> </p>

<div align="center"> 

**`Fig.11` Edit account form**

</div>

3.	Once the changes are done, Click on "Update" button to update the account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU16.jpg"> </p>

<div align="center"> 

**`Fig.12` Edit account**

</div>

4.	On successful Account update, toasted messages will be shown as "Account updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU17.jpg"> </p>

<div align="center"> 

**`Fig.13` Edit account**

</div>

5.	To discard the edit process, Super administrator can click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU18.jpg"> </p>

<div align="center"> 

**`Fig.14` Cancel edit account**

</div>

### Account Delete

1.	To delete 'Account' hover on the account, delete button is visible.

<p align="center"> <img width="1000" height="450" src="media/SU/SU19.jpg"> </p>

<div align="center"> 

**`Fig.16` Delete account**

</div>

2.	Once click on "Delete" button, application shows the conformation pop up, then click on "yes"

<p align="center"> <img width="1000" height="450" src="media/SU/SU20.jpg"> </p>

<div align="center"> 

**`Fig.17` Delete account**

</div>

3.	After click on Yes button, application shows the toasted message as "Account deleted successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU21.jpg"> </p>

<div align="center"> 

**`Fig.18` Account delete message**

</div>

4.	If click on 'No', Application shows accounts page and account will not be deleted.

5.	Account cannot be deleted if account is associated with any administrator. And shows the message as Account cannot be deleted, as it is associated with "Admin username".

<p align="center"> <img width="1000" height="450" src="media/SU/SU22.jpg"> </p>

<div align="center"> 

**`Fig.19` Alert message for accosiated account delete**

</div>

### Account disassociation

1.	To remove any Administrators from a particular Account, Super administrator need to select the Account and click on disassociation button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU23.jpg"> </p>

<div align="center"> 

**`Fig.20` Account disassociation**

</div>

2.	If admins are not associated, then it shows "No data available".

3.	Once click Admins dropdown, we can see the list of Administrators Associated, Select the type as Admins in right-hand top dropdown.

<p align="center"> <img width="1000" height="450" src="media/SU/SU24.jpg"> </p>

<div align="center"> 

**`Fig.21` Account disassociation**

</div>

4.	If admins are available, then it shows the list of admins available.

5.	To disassociate Admin, Select the Username and click on "Remove" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU25.jpg"> </p>

<div align="center"> 

**`Fig.22` List of admin in account disassociate page**

</div>

6.	On clicking on Remove it shows the tosted message as "Admin removed successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU26.jpg"> </p>

<div align="center"> 

**`Fig.23` Admin removed message**

</div>

7.	In between if Super administrator wants to cancel the disassociation of Admin, click on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU27.jpg"> </p>

<div align="center"> 

**`Fig.24` Cancel disassociation of account**

</div>

### Account Search

1.	Super administrator can search an Account by providing Account name.

2.	In the right-hand top corner, we can see search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU4.jpg"> </p>

<div align="center"> 

**`Fig.25` Account search**

</div>

3.  Provide Account name in the search box and click on → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU5.jpg"> </p>

<div align="center"> 

**`Fig.26` Search Account**

</div>

4.	Shows the result as per input provided in search box.

<p align="center"> <img width="1000" height="450" src="media/SU/SU6.jpg"> </p>

<div align="center"> 

**`Fig.27` Search Account**

</div>