﻿

Templates are created by super administrator, so that the account administrator can import the template as form and then modify and use according to their requirement for their projects/work assignments. 

1.	When clicked on Templates tab, displays list of Templates available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU127.jpg"> </p>

<div align="center"> 

**`Fig.127` Templates tab**

</div>

2.	Super administrator can create, update, preview, delete Templates.

### Create template

1.  On the right-hand top corner, 'ellipse' button is visible in templates page. 

2.	On Click of "create" button in ellipse, Super administrator can create new 'Template'.

<p align="center"> <img width="1000" height="450" src="media/SU/SU131.jpg"> </p>

<div align="center">

**`Fig.128` Create templates**

</div>

3.	A form will be opened with following fields:

 i. Name, 

 ii. Category, 

 iii. Template Type.

<p align="center"> <img width="1000" height="450" src="media/SU/SU132.jpg"> </p>

<div align="center"> 

**`Fig.129` Create templates**

</div>

Template types are

 - Private

 - Public

Private: If private selected, then the template will be visible to only selected Accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU133.jpg"> </p>

<div align="center"> 

**`Fig.130` Create templates**

</div>

Public: If public selected, then the template will be visible to all Accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU134.jpg"> </p>

<div align="center"> 

**`Fig.131` Create templates**

</div>

1.	Creation of Public and Private template

2.	After clicking of 'Go' button for public template

<p align="center"> <img width="1000" height="450" src="media/SU/SU135.jpg"> </p>

<div align="center"> 

**`Fig.132` Create templates**

</div>

3.	It will open 'Form Builder' where sper user can drag and drop different widgets.

<p align="center"> <img width="1000" height="450" src="media/SU/SU136.jpg"> </p>

<div align="center"> 

**`Fig.133` Create templates**

</div>

4. Drag and drop the widgets to the pallate to create Template.

-	Following are the types of widgets and fields:

`Form Widgets`

 - Input

 - Text area

 - Dropdown

 - Checkbox

 - Radio

 - Number

 - Rating

 - Calendar

`Media Widgets`

 - Camera

 - Video

 - Signature

 - Barcode

`Add on Widgets`

 - Calculator

 - Header

 - Map

 - Table

 - Time

 - Properties

 - Reference List

<p align="center"> <img width="1000" height="450" src="media/SU/SU137.jpg"> </p>

<div align="center"> 

**`Fig.134` Create templates**

</div>

5.	After drag and drop of widget, we need to fill the properties of the widget and need to save the property.

<p align="center"> <img width="1000" height="450" src="media/SU/SU138.jpg"> </p>

<div align="center"> 

**`Fig.135` Create templates**

</div>

6.	After saving of the widget Super administrator need to save the Template by clicking the 'save' icon in the top right corner.

<p align="center"> <img width="1000" height="450" src="media/SU/SU139.jpg"> </p>

<div align="center"> 

**`Fig.136` Create templates**

</div>

7.	After clicking of the save icon a popup opens with a drop down, select the Display fields and save the Template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU140.jpg"> </p>

<div align="center"> 

**`Fig.137` Create templates**

</div>

8.	Select the display fields

<p align="center"> <img width="1000" height="450" src="media/SU/SU141.jpg"> </p>

<div align="center"> 

**`Fig.138` Create templates**

</div>

9.	Then click on green tick mark to create template.

10.	On click on green tick icon, toasted message will be displays as "Template created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU142.jpg"> </p>

<div align="center"> 

**`Fig.139` Template created message**

</div>

!> Duplicate names not allowed.


</div>

11.	While creating process of template Super administrator can cancel the process and can navigates back to the templates page by Clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU144.jpg"> </p>

<div align="center"> 

**`Fig.140` Cancel template creation**

</div>

### Template preview

1.	Mouse hover on template card, the 'preview' icon is visible.

2.	On click 'preview' icon, template will be shown in read-only format.

<p align="center"> <img width="1000" height="450" src="media/SU/SU145.jpg"> </p>

<div align="center"> 

**`Fig.141` Preview of template**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU146.jpg"> </p>

<div align="center"> 

**`Fig.142` Preview of template**

</div>

### Edit template

1. To edit a Template, mouse hover on the Template, 'edit' icon is visible.

<p align="center"> <img width="1000" height="450" src="media/SU/SU147.jpg"> </p>

<div align="center"> 

**`Fig.143` Edit template**

</div>

2. On Click of edit button on the template card Super administrator can the see existing basic details.

<p align="center"> <img width="1000" height="450" src="media/SU/SU148.jpg"> </p>

<div align="center"> 

**`Fig.144` Edit template**

</div>

3.	After clicking of "Go" button. It will open 'Form Builder' with the existing widgets and can add new widgets to the Template.

4.	We need to drag and drop the widgets to the palate to add new widgets to Template.

5.	After drag and drop of widget, fill the properties of the widget and save the properties.

<p align="center"> <img width="1000" height="450" src="media/SU/SU149.jpg"> </p>

<div align="center"> 

**`Fig.145` Edit template**

</div>

6.	After saving of the widget, save the Template by clicking the 'save' icon in the top right corner.

<p align="center"> <img width="1000" height="450" src="media/SU/SU150.jpg"> </p>

<div align="center"> 

**`Fig.146` Edit template**

</div>

7.	After clicking of the save button, displays a popup with a drop down, select the Display fields and update the Template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU151.jpg"> </p>

<div align="center"> 

**`Fig.147` Edit template**

</div>
8.	On click update, Template gets updated.

<p align="center"> <img width="1000" height="450" src="media/SU/SU152.jpg"> </p>

<div align="center"> 

**`Fig.148` Edit template**

</div>

9.	On click cancel, Super administrator can navigate back to templates page without applying changes.

<p align="center"> <img width="1000" height="450" src="media/SU/SU154.jpg"> </p>

<div align="center"> 

**`Fig.149` Cancel edit template**

</div>

10.	Super administrator can copy the existing template by Clicking on "Copy" button with same widget with different form name.

Hover on template and click on edit option:

 - Click on Go button

 - Click on save template icon

 - Click on Copy button

> Super administrator can change private template to public and vice-versa. Super administrator can add or remove or modify the widgets.

<p align="center"> <img width="1000" height="450" src="media/SU/SU155.jpg"> </p>

<div align="center"> 

**`Fig.150` Copy template**

</div>

11.	On click copy button, opens copy template with following fields:

 - New form name

 - display fields

<p align="center"> <img width="1000" height="450" src="media/SU/SU156.jpg"> </p>

<div align="center"> 

**`Fig.151` Copy template**

</div>

12.	Enter new form name and select display fields.

13.	Click Copy button, Template will be copied successfully, and toasted message will be shown as "Template copied successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU157.jpg"> </p>

<div align="center"> 

**`Fig.152` Copy template**

</div>

14.	Super administrator can cancel the copy process by Clicking on cancel button, and Redirects back to the template update page.

<p align="center"> <img width="1000" height="450" src="media/SU/SU158.jpg"> </p>

<div align="center"> 

**`Fig.153` Cancel copy template**

</div>

### Template Delete

1.	To delete 'Template' hover on the template, to see the icons.

<p align="center"> <img width="1000" height="450" src="media/SU/SU159.jpg"> </p>

<div align="center"> 

**`Fig.154` Delete template**

</div>

2.	Once click on 'Delete' icon, application shows the confirmation pop up, then select the 'yes' option.

3.	After click on 'Yes' button, application shows the toast message as "template deleted successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU161.jpg"> </p>

<div align="center"> 

**`Fig.155` Delete template**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU162.jpg"> </p>

<div align="center"> 

**`Fig.156` Template deleted message**

</div>

If Super administrator clicks on "No", Then template delete process gets cancelled.



### Search template

1.	Super administrator can search the template by Template name.

<p align="center"> <img width="1000" height="450" src="media/SU/SU128.jpg"> </p>

<div align="center"> 

**`Fig.157` Search templates**

</div>

2.	Enter the template name in the search box and click on  → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU129.jpg"> </p>

<div align="center"> 

**`Fig.158` Search templates**

</div>

3.	List all the templates by given name in the search box.

4.	On success, result shows all the available templates with given name.

5.	If templates are not available then it shows the message as "No data available".


