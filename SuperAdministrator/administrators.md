﻿
Administrators can manage the data of an account and are involved in the workflow to review the data submitted by the field users.

There are three types of administrators:

 1.	Account Administrator

 2.	Group Administrator

 3.	Moderator

There can be only one account administrator and multiple group administrators, moderators under each account created by the Super administrator.

1.	On click of Administrators tab, Super administrator can see the list of Administrators available.

<p align="center"> <img width="1000" height="450" src="media/SU/SU43.jpg"> </p>

<div align="center"> 

**`Fig.28` list of administrators**

</div>

2.	Super administrator can search for Administrator, Moderator by Username.

3.	Super administrator can filter the Administrator by using filter in the right-hand top.

4.	Super administrator can see, create, update, delete, reset password for the following Administrators:

    - Account Administrators

	- Group Administrators

	- Moderators


### Administrator creation

1.	Super administrator can create the Administrator by clicking on the "create" button in ellipse on the right-hand top.

<p align="center"> <img width="1000" height="450" src="media/SU/SU50.jpg"> </p>

<div align="center"> 

**`Fig.29` Create administrator**

</div>

2.	On click of create a form will open fill the details and click on "create".

3.	We have Account Administrator, Group Administrator and Moderators.

4.	Based on the check box selection Administrators are created.

### Create Account Administrator

1. Application displays administrator creation page

<p align="center"> <img width="1000" height="450" src="media/SU/SU52.jpg"> </p>

<div align="center"> 

**`Fig.30` Account administrator creation**

</div>

2. Select Account administrator to create Account administrator.

<p align="center"> <img width="1000" height="450" src="media/SU/SU53.jpg"> </p>

<div align="center"> 

**`Fig.31` Account administrator creation**

</div>

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on create button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU54.jpg"> </p>

<div align="center"> 

**`Fig.32` Account administrator creation**

</div>

6. On successful creation, Shows the toasted message as "Account Administrator created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU55.jpg"> </p>

<div align="center"> 

**`Fig.33` Account administrator creation**

</div>

8. Super administrator can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU57.jpg"> </p>

<div align="center"> 

**`Fig.34` Cancel account administrator creation**

</div>


### Create Group administrator

1. Application displays administrator creation page.

<p align="center"> <img width="1000" height="450" src="media/SU/SU52.jpg"> </p>

<div align="center"> 

**`Fig.35` Group administrator creation**

</div>

2. Select Group administrator to create group administrator

<p align="center"> <img width="1000" height="450" src="media/SU/SU68.jpg"> </p>

<div align="center"> 

**`Fig.36` Group administrator creation**

</div>

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on "create" button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU69.jpg"> </p>

<div align="center"> 

**`Fig.37` Group administrator creation**

</div>

6. On successful creation, Shows the toasted messages as "Group Administrator created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU70.jpg"> </p>

<div align="center"> 

**`Fig.38` Group administrator creation**

</div>

7. Super administrator can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU72.jpg"> </p>

<div align="center"> 

**`Fig.39` Cancel group administrator creation**

</div>

### Create moderator

1. Application displays administrator creation page.

<p align="center"> <img width="1000" height="450" src="media/SU/SU52.jpg"> </p>

<div align="center"> 

**`Fig.40` Moderator creation**

</div>

2. If Role is not selected, then Moderator will be created.

3. Select account to associate administrator to accounts.

4. Select avatar and upload image for easy identification.

5. Click on "create" button to create account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU82.jpg"> </p>

<div align="center"> 

**`Fig.41` Moderator creation**

</div>

6. On successful creation, Shows the toasted messages as "Moderator created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU84.jpg"> </p>

<div align="center"> 

**`Fig.42` Moderator creation**

</div>

7. Super administrator can Cancel the creation process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU73.jpg"> </p>

<div align="center"> 

**`Fig.43` Cancel moderator creation**

</div>


### Edit administrator

1. Mouse hover on admin card to see the 'Edit' icon and click on Edit icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU73.jpg"> </p>

<div align="center"> 

**`Fig.45` Edit group administrator**

</div>

2. On click Edit icon, edit page will be opened.


<p align="center"> <img width="1000" height="450" src="media/SU/SU74.jpg"> </p>

<div align="center"> 

**`Fig.46` Edit group administrator**

</div>

3. After modifications, Click on update icon

> User can only add the new accounts

<p align="center"> <img width="1000" height="450" src="media/SU/SU75.jpg"> </p>

<div align="center"> 

**`Fig.47` Update group administrator**

</div>

4. On click "Update" button, Toasted message will be shown as "Admin updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU76.jpg"> </p>

<div align="center"> 

**`Fig.48` Admin updated message**

</div>

5. Super administrator can Cancel the update process by clicking on cancel button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU78.jpg"> </p>

<div align="center"> 

**`Fig.49` Cancel edit group administrator**

</div>

### Delete administrator

1. If Administrator is associated with account then group administrator cannot be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU79.jpg"> </p>

<div align="center"> 

**`Fig.50` Delete group administrator**

</div>

2. If Group administrator is not associated with any account, then group administrator can be deleted.

<p align="center"> <img width="1000" height="450" src="media/SU/SU80.jpg"> </p>

<div align="center"> 

**`Fig.51` Delete group administrator**

</div>

## Administrator password reset

1.	Super administrator can reset the password for Account Administrator, Group Administrators, and Moderator to default password.

2.	Mouse hover on Administrator card and click on password reset icon.

3.	Default password is 'mm@1234'.

<p align="center"> <img width="1000" height="450" src="media/SU/SU95.jpg"> </p>

<div align="center"> 

**`Fig.52` Reset administrator password**

</div>

4.	Once click on 'Reset' icon, application shows the conformation pop up, then select the 'yes' option.

5.	After click on 'Yes' button, application shows the toasted message as "Admin password reset successful".

<p align="center"> <img width="1000" height="450" src="media/SU/SU95-1.jpg"> </p>

<div align="center"> 

**`Fig.53` Password reset toast message**

</div>

### Administrator search

1.	Super administrator can search by providing username in the search box.

2.	Enter the username and click on → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU44.jpg"> </p>

<div align="center"> 

**`Fig.54` Search administrators**

</div>

3.	Application displays search results.

<p align="center"> <img width="1000" height="450" src="media/SU/SU45.jpg"> </p>

<div align="center"> 

**`Fig.55` Search administrators**

</div>

### Filter administrators

1.	Super administrator can filter the administrators with Administrator type.

2.	Click on filter and select "All", Shows all the Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU46.jpg"> </p>

<div align="center"> 

**`Fig.56` Filter administrators**

</div>

3.	Click on filter and select "Account Administrator", Shows all the Account Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU47.jpg"> </p>

<div align="center"> 

**`Fig.57` Filter account administrators**

</div>

4.	Click on filter and select "Group Administrator", Shows all the Group Administrators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU48.jpg"> </p>

<div align="center"> 

**`Fig.58` Filter group administrators**

</div>

5.	Click on filter and select "Moderator", Shows all the Moderators.

<p align="center"> <img width="1000" height="450" src="media/SU/SU49.jpg"> </p>

<div align="center"> 

**`Fig.59` Filter moderators**

</div>