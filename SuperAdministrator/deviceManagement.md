


1.	Once the Super administrator logs in, displays "Device Management" icon in left side menu.

 -	All the devices belonging to all the accounts will be visible on clicking on "Device Management" tab.

 -	Any user trying to login form any device for the first time, the device has to be approved by the Super administrator.

 -	When the request for a new device comes, its status is "Pending". Super administrator needs to check for the credibility and take necessary action.

<p align="center"> <img width="1000" height="450" src="media/SU/SU219.jpg"> </p>

<div align="center"> 

**`Fig.87` Device management page**

</div>

 -	We get the list of "Pending" devices from the device icon in the header.

 -	Super administrator can Approve/Suspend/Revoke/Unauthorize a particular device request based on the credibility.

2.	Super administrator can apply filter bases on status

The following are the status:

 i.	All

 ii. Approved

 iii. Pending

 iv. Rejected

 v. Suspended

 vi. Unauthorized

<p align="center"> <img width="1000" height="450" src="media/SU/SU220.jpg"> </p>

<div align="center"> 

**`Fig.88` Device management page**

</div>

-	If Super administrator selects "All" option from filter

<p align="center"> <img width="1000" height="450" src="media/SU/SU221.jpg"> </p>

<div align="center"> 

**`Fig.89` Filter device details**

</div>

-	Super administrator can see all the devices with its status as Pending/Rejected/Approved/Suspended/Unauthorized.

-	If Super administrator selects "Approved", then Super administrator can see all the approved devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU222.jpg"> </p>

<div align="center"> 

**`Fig.90` Filter device details**

</div>

-	If Super administrator selects "Pending", then Super administrator can see all the pending devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU223.jpg"> </p>

<div align="center"> 

**`Fig.91` Filter device details**

</div>

-	If Super administrator selects "Rejected", then Super administrator can see all the rejected devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU224.jpg"> </p>

<div align="center"> 

**`Fig.92` Filter device details**

</div>

-	If Super administrator selects "suspended", then Super administrator can see all the suspended devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU225.jpg"> </p>

<div align="center"> 

**`Fig.93` Filter device details**

</div>

-	If Super administrator selects "unauthorized", then Super administrator can see all the unauthorized devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU226.jpg"> </p>

<div align="center"> 

**`Fig.94` Filter device details**

</div>

3.	Super administrator can approve the device

-	If Super administrator, click on status of device, it shows the list as approve, Reject, Unauthorize.

<p align="center"> <img width="1000" height="450" src="media/SU/SU227.jpg"> </p>

<div align="center"> 

**`Fig.95` Device management page**

</div>

-	If Super administrator clicks on Approve, it shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU228.jpg"> </p>

<div align="center"> 

**`Fig.96` Approve Device**

</div>

-	If Super administrator clicks on "Yes" then the device will get approved and toasted message will be shown as "Device is approved" and send auto-generate approval mail to field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU229.jpg"> </p>

<div align="center"> 

**`Fig.97` Approve device**

</div>

-	The status will be changed from Pending to Approved.

-	If Super administrator clicks on "No" then the device will not get approved and shows a toasted message as "Device approval action is cancelled"

<p align="center"> <img width="1000" height="450" src="media/SU/SU230.jpg"> </p>

<div align="center"> 

**`Fig.98` Cancel approve device**

</div>

5.	Super administrator can reject the device details

 -	On click status dropdown, Super administrator can see Approve, Reject, unauthorized.

 -	On selection of Reject, Super administrator can reject the device details. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU231.jpg"> </p>

<div align="center"> 

**`Fig.99` Reject device**

</div>

-	On click reject, Super administrator can see the rejection alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU232.jpg"> </p>

<div align="center"> 

**`Fig.100` Reject device**

</div>

-	Shows the alert to confirm the device details

-	If Super administrator clicks on "Yes"

<p align="center"> <img width="1000" height="450" src="media/SU/SU233.jpg"> </p>

<div align="center"> 

**`Fig.101` Reject device**

</div>

-	Device Rejected Successfully

-	If device status is rejected then Super administrator can delete the device details.

-	Click on delete icon, then shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU234.jpg"> </p>

<div align="center"> 

**`Fig.102` Delete device details**

</div>

-	If click on "Yes", then device details gets deleted, and toasted message shown as "Device deleted successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU235.jpg"> </p>

<div align="center"> 

**`Fig.103` Delete device details**

</div>

-	If click on "No", then device details gets rejected status and toasted message shown as "Device deletion is cancelled" and all the device details page will be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU236.jpg"> </p>

<div align="center"> 

**`Fig.104` Cancel delete device details**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU237.jpg"> </p>

<div align="center"> 

**`Fig.105` Device management page**

</div>

-	Device Reject process will be cancelled.

5.	Super administrator can Unauthorized the device

 -	Super administrator need to click on status dropdown

 -	Super administrator can unauthorize the device details by selecting unauthorize option


<p align="center"> <img width="1000" height="450" src="media/SU/SU238.jpg"> </p>

<div align="center"> 

**`Fig.106` Unauthorize Device**

</div>

-	On click Unauthorized, Device Unauthorize process shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU239.jpg"> </p>

<div align="center"> 

**`Fig.107` Unauthorize Device**

</div>

-	If Super administrator clicks on "No", Device Unauthorize process gets cancelled

<p align="center"> <img width="1000" height="450" src="media/SU/SU240.jpg"> </p>

<div align="center"> 

**`Fig.108` Cancel Unauthorize Device**
</div>

-	If Super administrator clicks on "Yes", Device gets unauthorized, and device can be deleted by clicking on delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU241.jpg"> </p>

<div align="center"> 

**`Fig.109` Device management page**

</div>

-	Super administrator can delete the unauthorized devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU242.jpg"> </p>

<div align="center"> 

**`Fig.110` Delete device details**

</div>

-	On click delete icon, Deletion confirmation alert gets displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU243.jpg"> </p>

<div align="center"> 

**`Fig.111` Delete device details**

</div>

-	If click on "Yes", then device gets deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/SU244.jpg"> </p>

<div align="center"> 

**`Fig.112` Delete device details**

</div>

-	If click on "No", deletion process gets cancelled.

6.	Super administrator can suspend the device details.

If the device status is approved and Super administrator wants to Suspend then Super administrator clicks on Approve and it shows the suspend option as follows

<p align="center"> <img width="1000" height="450" src="media/SU/SU245.jpg"> </p>

<div align="center"> 

**`Fig.113` Device management page**

</div>

- If Super administrator clicks on Suspend.

<p align="center"> <img width="1000" height="450" src="media/SU/SU246.jpg"> </p>

<div align="center"> 

**`Fig.114` Suspend Device**

</div>

- If Super administrator clicks on "No", process gets cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU247.jpg"> </p>

<div align="center"> 

**`Fig.115` Cancel suspend Device**

</div>

- If Super administrator clicks on "Yes", device gets suspended

<p align="center"> <img width="1000" height="450" src="media/SU/SU248.jpg"> </p>

<div align="center"> 

**`Fig.116` Suspend Device**

</div>

- Changes the device status gets changed from Approved to suspended.

<p align="center"> <img width="1000" height="450" src="media/SU/SU249.jpg"> </p>

<div align="center"> 

**`Fig.117` Device management page**

</div>


- Super administrator can delete the suspended devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/suspended devices.jpg"> </p>

<div align="center"> 

**`Fig.118` Delete device details**

</div>

-	On click delete icon, Deletion confirmation alert gets displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/delete device.jpg"> </p>

<div align="center"> 

**`Fig.119` Delete device details**

</div>

-	If click on "Yes", then device gets deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/deleted toast.jpg"> </p>

<div align="center"> 

**`Fig.120` Delete device details**

</div>

-	If click on "No", deletion process gets cancelled.

7.	Super administrator can update the status from suspend to revoke back to pending.

<p align="center"> <img width="1000" height="450" src="media/SU/SU250.jpg"> </p>

<div align="center"> 

**`Fig.121` Revoke device status**

</div>

- Click on revoke, shows the conformation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU251.jpg"> </p>

<div align="center"> 

**`Fig.122` Revoke device status**

</div>

- If click on "No", revoke process gets cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU252.jpg"> </p>

<div align="center"> 

**`Fig.123` Cancel revoke device**

</div>

- If click on "Yes", device details gets revoked successfully, and toasted message displayed as "your device is pending".

<p align="center"> <img width="1000" height="450" src="media/SU/SU253.jpg"> </p>

<div align="center"> 

**`Fig.124` Device management page**

</div>

- when a suspended device is revoke, status changes to pending. 

## Device Management Activity

1. Click on activity tab in device management page.

2. Application will open activity page of device management with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/SU/device management.jpg"></p>

<div align="center">

**`Fig.125` Device activity filter** 

</div>

3. Choose From date and To date, account dropdown and click on filter icon.

4. Device Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/SU/device management activity filter.jpg"></p>

<div align="center">

**`Fig.126` Device activity filter**

</div>
