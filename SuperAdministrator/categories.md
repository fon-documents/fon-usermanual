﻿

Categories are created to group the forms and templates based on its category. On choosing a particular category while creating a project/ task, so that administrator can only use forms with the selected category for assignments.

1.	When clicked on Categories tab, we can see the list of Categories available.

2.	Super administrator and Account Administrator can create Categories. 

<p align="center"> <img width="1000" height="450" src="media/SU/SU108.jpg"> </p>

<div align="center"> 

**`Fig.71` Categories tab**

</div>

### Category creation

1.  On the right-hand top corner, ellipse button is displayed in category page. 

2.	On Click of "create" button in ellipse we can create new 'Category'.

3.	A form will be opened with name and description.

<p align="center"> <img width="1000" height="450" src="media/SU/SU110.jpg"> </p>

<div align="center"> 

**`Fig.72` Create category**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU111.jpg"> </p>

<div align="center"> 

**`Fig.73` Create category**

</div>

4.	Fill the details and click the create button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU112.jpg"> </p>

<div align="center"> 

**`Fig.74` Create category**

</div>

5.	After successful creation, Super administrator can see a toasted message as "Category created successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU113.jpg"> </p>

<div align="center"> 

**`Fig.75` Category created message**

</div>

!> Duplicate names not allowed.

6.	Click on "cancel" button to cancel the process.

<p align="center"> <img width="1000" height="450" src="media/SU/SU114.jpg"> </p>

<div align="center"> 

**`Fig.76` Cancel category creation**

</div>

### Edit category

1.	To edit Category, hover on the category, Super administrator can see the following buttons:
  - Edit
  - Delete

<p align="center"> <img width="1000" height="450" src="media/SU/SU116.jpg"> </p>

<div align="center"> 

**`Fig.77` Edit category**

</div>

2.	Click on Edit button, application displays edit category form.

<p align="center"> <img width="1000" height="450" src="media/SU/SU117.jpg"> </p>

<div align="center"> 

**`Fig.78` Edit category**

</div>

3.  After modifications, click on "update" button to update category.

<p align="center"> <img width="1000" height="450" src="media/SU/SU119.jpg"> </p>

<div align="center"> 

**`Fig.79` Edit category**

</div>

4.  On click "update" button, it shows the message as "category updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU120.jpg"> </p>

<div align="center"> 

**`Fig.80` Category updated message**

</div>

5.  In between if Super administrator wants to cancel the update process, then Super administrator can cancel the update process by clicking "Cancel" button.
 
<p align="center"> <img width="1000" height="450" src="media/SU/SU121.jpg"> </p>

<div align="center"> 

**`Fig.81` Cancel edit category**

</div>

### Delete category

1.	To delete Category, hover on the category to see the buttons.

<p align="center"> <img width="1000" height="450" src="media/SU/SU123.jpg"> </p>

<div align="center"> 

**`Fig.82` Delete category**

</div>

2.	Once click on 'Delete' icon, application shows the conformation pop up, then select the 'yes' option.

<p align="center"> <img width="1000" height="450" src="media/SU/SU124.jpg"> </p>

<div align="center"> 

**`Fig.83` Delete category**

</div>

3.	After click on 'Yes' button, application shows the toasted message as "category deteled successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU125.jpg"> </p>

<div align="center"> 

**`Fig.84` Delete category**

</div>

4.	Cannot delete if category is associated with template.

<p align="center"> <img width="1000" height="450" src="media/SU/SU126.jpg"> </p>

<div align="center"> 

**`Fig.85` Delete category**

</div>

### Search category

1.	Super administrator can search category by providing category name in search box.

2.	Enter category name in search box and then click on  → icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU109.jpg"> </p>

<div align="center"> 

**`Fig.86` Search category**

</div>

