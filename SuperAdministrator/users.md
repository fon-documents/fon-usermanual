﻿

Users are the workers who work on the assigned task/assignment and submit the data in the respective forms/assignments assigned to them from the field using mobile.

Super administrator can search, update, check the activities of a Field user.


### Edit user

Edit user function defines, super administrator can edit/ update the account for field user.

1.	To edit the user profile, Mouse hover on user card.

2.	Edit icon will be appeared.

<p align="center"> <img width="1000" height="450" src="media/SU/SU98.jpg"> </p>

<div align="center"> 

**`Fig.60` Edit users**

</div>


3.	On Click 'Edit' icon, Opens the edit page. 

4.	Super administrator has only Accounts field as editable and Super administrator can add accounts.

<p align="center"> <img width="1000" height="450" src="media/SU/SU99.jpg"> </p>

<div align="center"> 

**`Fig.61` Edit users**

</div>

5.  After adding the account, Super administrator can click on "update" button to update the field user to specific account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU100.jpg"> </p>

<div align="center"> 

**`Fig.62` Edit users**

</div>

6.  On successful update, toasted message will be shown as "User updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU101.jpg"> </p>

<div align="center"> 

**`Fig.63` User updated message**

</div>

7.  To cancel the process, Super administrator can click on "Cancel" button.

<p align="center"> <img width="1000" height="450" src="media/SU/SU102.jpg"> </p>

<div align="center"> 

**`Fig.64` Cancel edit users**

</div>

### Search user

1.	Super administrator can search for a Field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU96.jpg"> </p>

<div align="center"> 

**`Fig.65` Search users**

</div>

2.  Click on the search box and enter the username.

3.  Click on  → icon, then it shows the result.

<p align="center"> <img width="1000" height="450" src="media/SU/SU97.jpg"> </p>

<div align="center"> 

**`Fig.66` Search users**

</div>

### Users Activity

1.  Click on Activity tab.

<p align="center"> <img width="1000" height="450" src="media/SU/SU104.jpg"> </p>

<div align="center"> 

**`Fig.67`  Users Activity tab**

</div>

2.  Select From date, To date and select Account.

<p align="center"> <img width="1000" height="450" src="media/SU/SU105.jpg"> </p>

<div align="center"> 

**`Fig.68` Users activity**

</div>

3.  If Super administrator selects Accounts as "All" and click on 'Filter' icon, Shows all the activities with Activity, Username, Date, Device model and version.

<p align="center"> <img width="1000" height="450" src="media/SU/SU106.jpg"> </p>

<div align="center"> 

**`Fig.69` User activities of all accounts**

</div>

4.  If Super administrator selects Accounts as "a specific account" and click on Filter icon, Shows all the activities with Activity, Username, Date, Device model and version.

<p align="center"> <img width="1000" height="450" src="media/SU/SU107.jpg"> </p>

<div align="center"> 

**`Fig.70` User activity of an account**

</div>
