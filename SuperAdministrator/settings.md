﻿

Super administrator will be able to perform some of the actions from the system. The below is the list:

 -	Profile

 -	Privileges

 -	Application Settings

 -	Accounts Settings

 -  Map configuration

 -	Layer configuration

<p align="center"> <img width="1000" height="450" src="media/SU/1.jpg"> </p>

<div align="center"> 

**`Fig.174` Settings page**

</div>

###	Profile

-	Super administrator will be able to change the user details like 'First name, Last name, Email, Phone Number, Image'.

-	First, click on the profile and then edit the required details and then click on "save" button. The changes gets updated with new data.

<p align="center"> <img width="1000" height="450" src="media/SU/1-1.jpg"> </p>

<div align="center"> 

**`Fig.175` profile settings**

</div>

-	The application displays a message saying, "Profile Updated Successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/2.jpg"> </p>

<div align="center"> 

**`Fig.176` Update profile**

</div>

### Privileges

-	Click on Privileges to update Admin privileges.

<p align="center"> <img width="1000" height="450" src="media/SU/1-2.jpg"> </p>

<div align="center"> 

**`Fig.177` View privileges**

</div>

-   Select admin by clicking on admins dropdown.

<p align="center"> <img width="1000" height="450" src="media/SU/3.jpg"> </p>

<div align="center"> 

**`Fig.178` Update privileges**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/4.jpg"> </p>

<div align="center"> 

**`Fig.179` Update privileges**

</div>

-	On selecting admin and type, the list of menu options with toggle off and on are shown as follows:

 ◦	Downloads

 ◦	Forms

 ◦	Templates

 ◦	Category

 ◦	Projects

 ◦	Tasks

 ◦	Users

 ◦	Administrators

 ◦	Workflow management

 ◦	Device management

 ◦	Settings

<p align="center"> <img width="1000" height="450" src="media/SU/5.jpg"> </p>

<div align="center"> 

**`Fig.180` Update privileges**

</div>

-	On click update, privileges gets updated and toasted message shown as "Privileges set successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/2-1.jpg"> </p>

<div align="center"> 

**`Fig.181` Update privileges**

</div>


#### Application Settings

1.	Super administrator can update Application settings.

2.	To update Account settings, click on Application setting icon.

<p align="center"> <img width="1000" height="450" src="media/SU/1-3.jpg"> </p>

<div align="center"> 

**`Fig.182` Application settings**

</div>

3.	On click application settings, Super administrator can see the application settings page with following options:

  a.	Application security toggle

  b.	A table with following headers

     i.	Parameter

     ii. Value

     iii. Description

     iv. Action

<p align="center"> <img width="1000" height="450" src="media/SU/6.jpg"> </p>

<div align="center"> 

**`Fig.183` Application settings**

</div>

-	On click application security toggle on, Super administrator can on the security, and it shows the message as "Settings updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/7.jpg"> </p>

<div align="center"> 

**`Fig.184` Application settings**

</div>


- Super administrator can set the value for number of unsuccessful attempts after which account will be locked in account lock settings.

-	Super administrator can update, reset or cancel the application settings process.

 o	Click on update in actions for Account lock settings

 o	Value will show as editable field

 o	Super administrator can change the value and click on update

 o	On successful update, toasted message will be shown as "settings updated successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/8.jpg"> </p>

<div align="center"> 

**`Fig.185` Account lock settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/9.jpg"> </p>

<div align="center"> 

**`Fig.186` Account lock settings**

</div>

o	To rest with default value, Super administrator has to click on update and then click on Reset.

<p align="center"> <img width="1000" height="450" src="media/SU/10.jpg"> </p>

<div align="center"> 

**`Fig.187` Account lock settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/11.jpg"> </p>

<div align="center"> 

**`Fig.188` Account lock settings**

</div>

- Super administrator can set value for minimum time locked for unsuccessful attempts in lock interval settings.

o	Click on update in actions for Lock interval settings

o	Value will show as editable field

o	Super administrator can change the value and click on update

o	On successful update, toasted message shown as "settings updated successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/12.jpg"> </p>

<div align="center"> 

**`Fig.189` Lock interval settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/13.jpg"> </p>

<div align="center"> 

**`Fig.190` Lock interval settings**

</div>

o	To rest with default value, Super administrator must click on update and then click on "Reset".

<p align="center"> <img width="1000" height="450" src="media/SU/14.jpg"> </p>

<div align="center"> 

**`Fig.191` Lock interval settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/15.jpg"> </p>

<div align="center"> 

**`Fig.192` Lock interval settings**

</div>

- Super administrator can set value for user idle session timeout limit in idle time settings, where the user gets logged out automatically if the application is not in use for the set time.

Click on update in actions for idle time settings

o	Value will show as editable field

o	Super administrator can change the value and click on update

o	On successful update, toasted message shown as "settings updated successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/16.jpg"> </p>

<div align="center"> 

**`Fig.193` Idle time settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/17.jpg"> </p>

<div align="center"> 

**`Fig.194` Idle time settings**

</div>

o	To rest with default value, Super administrator must click on update and then click on "Reset".

<p align="center"> <img width="1000" height="450" src="media/SU/18.jpg"> </p>

<div align="center"> 

**`Fig.195`  Idle time settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/19.jpg"> </p>

<div align="center"> 

**`Fig.196` Idle time settings**

</div>


- Super administrator can set maximum number of files allowed per record, where field user will be able to attach only the number of file applied in files per record settings.

o	Click on update in actions for files per record

o	Value will show as editable field

o	Super administrator can change the value and click on "update"

o	On successful update, toasted message shown as "settings updated successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/20.jpg"> </p>

<div align="center"> 

**`Fig.197` Files per record settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/21.jpg"> </p>

<div align="center"> 

**`Fig.152` Files per record settings**

</div>

o	To rest with default value, Super administrator must click on update and then click on "Reset".

<p align="center"> <img width="1000" height="450" src="media/SU/22.jpg"> </p>

<div align="center"> 

**`Fig.198` Files per record settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/23.jpg"> </p>

<div align="center"> 

**`Fig.199` Files per record settings**

</div>

- Super administrator can set value for maximum size of each file allowed per record, where the field user can attach the files upto the size mentioned in the size of file settings.

o	Click on update in actions for size of file

o	Value will show as editable field

o	Super administrator can change the value and click on update

o	On successful update, toasted message shown as "settings updated successfully"

<p align="center"> <img width="1000" height="450" src="media/SU/24.jpg"> </p>

<div align="center"> 

**`Fig.200` Size of file settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/25.jpg"> </p>

<div align="center"> 

**`Fig.201` Size of file settings
</div>

o	To rest with default value, Super administrator must click on update and then click on Reset.

<p align="center"> <img width="1000" height="450" src="media/SU/26.jpg"> </p>

<div align="center"> 

**`Fig.202` Size of file settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/27.jpg"> </p>

<div align="center"> 

**`Fig.203` Size of file settings**

</div>

### Account settings

1.	Super administrator can change the account settings by clicking on Account settings icon.

<p align="center"> <img width="1000" height="450" src="media/SU/1-4.jpg"> </p>

<div align="center"> 

**`Fig.204` Account settings**

</div>

2.	On click Account settings icon, Account setting page gets opened with Accounts dropdown and Account lock toggle.

<p align="center"> <img width="1000" height="450" src="media/SU/28.jpg"> </p>

<div align="center"> 

**`Fig.205` Account settings**

</div>

3.	From dropdown list, select account and click on update.

<p align="center"> <img width="1000" height="450" src="media/SU/30.jpg"> </p>

<div align="center"> 

**`Fig.206` Update account settings**

</div>

4.	On successful update, toasted message shown as setting updated successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/31.jpg"> </p>

<div align="center"> 

**`Fig.207` Account settings**

</div>


### Map configurations

-	Super administrator can update Maps for specific account.

<p align="center"> <img width="1000" height="450" src="media/SU/1-5.jpg"> </p>

<div align="center"> 

**`Fig.208` View map settings**

</div>

-	On click Maps option, Maps page gets open.

-	The following fields and options are displayed:

 ◦	Account – Dropdown

 ◦	Map with different options as follows

  ▪	Open street maps

  ▪	Map Box

  ▪ Google maps

<p align="center"> <img width="1000" height="450" src="media/SU/32.jpg"> </p>

<div align="center"> 

**`Fig.209` View map settings**

</div>

-	Super administrator must select account, then must select map component.

-	After selection Super administrator must click on update to apply the changes.

<p align="center"> <img width="1000" height="450" src="media/SU/33.jpg"> </p>

<div align="center"> 

**`Fig.210` Update map settings**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/34.jpg"> </p>

<div align="center"> 

**`Fig.211` Update map settings**

</div>

-	On success, Super administrator can see the toasted messages as "map updated successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/35.jpg"> </p>

<div align="center"> 

**`Fig.212` Update map settings**

</div>
 


### Layer configuration
 
- Super administrator can perform layer configuration settings by clicking on layer configuration.

<p align="center"> <img width="1000" height="450" src="media/SU/settings page.jpg"> </p>

<div align="center"> 

**`Fig.213` Settings page**

</div>

1. Create Layer

- Application opens layer configuration page with create layer and Accounts dropdown.

- Click on +/create layer button.

<p align="center"> <img width="1000" height="450" src="media/SU/layer configuration page.jpg"> </p>

<div align="center"> 

**`Fig.214` Layer configuration settings page**

</div>

- Add new layer form page opens with Accounts, external name, internal name, layer url, layer type and create, cancel buttons.

<p align="center"> <img width="1000" height="450" src="media/SU/add layer.jpg"> </p>

<div align="center"> 

**`Fig.215` Add new layer page**

</div>

- Select account from the dropdown list, enter details, select layer type wms/wmts and click on create button.

<p align="center"> <img width="1000" height="450" src="media/SU/create layer.jpg"> </p>

<div align="center"> 

**`Fig.216` Add new layer page**

</div>

- Application displays toast message for layer creation.

<p align="center"> <img width="1000" height="450" src="media/SU/create layer toast.jpg"> </p>

<div align="center"> 

**`Fig.217` Toast for layer creation**

</div>

2. Edit Layer

- In layer configuration page select account from the dropdown list, shows the existing layers of the selected account.

<p align="center"> <img width="1000" height="450" src="media/SU/layers list.jpg"> </p>

<div align="center"> 

**`Fig.218` Layers list**

</div>

- Click on edit layer option, opens edit layer form with layer url, layer type in editable mode and update, cancel button.

- Update the required changes and click on update button.

<p align="center"> <img width="1000" height="450" src="media/SU/add layer.jpg"> </p>

<div align="center"> 

**`Fig.219` Update layer**

</div>

- Application displays toast message for layer update.

<p align="center"> <img width="1000" height="450" src="media/SU/layer update toast.jpg"> </p>

<div align="center"> 

**`Fig.220` Update layer toast**

</div>

3. Preview layer

- Click on preview layer option.

- Opens map showing layers data on map. 

<p align="center"> <img width="1000" height="450" src="media/SU/layer preview.jpg"> </p>

<div align="center"> 

**`Fig.221` Layer preview**

</div>

4.  Search on map 

- Click on search icon on the map, search bar gets displayed.

- Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.222` Search on map**

</div>

- Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.223` Search on map**

</div>

5. Delete layer

- Click on delete layer icon.

<p align="center"> <img width="1000" height="450" src="media/SU/delete layer option.jpg"> </p>

<div align="center"> 

**`Fig.224` Delete layer**

</div>

- Application displays confirm action message, click on yes.

<p align="center"> <img width="1000" height="450" src="media/SU/delete action.jpg"> </p>

<div align="center"> 

**`Fig.225` Delete action message**

</div>

- Application displays toast message for layer delete.

<p align="center"> <img width="1000" height="450" src="media/SU/delete layer toast.jpg"> </p>

<div align="center"> 

**`Fig.226` Toast for delete layer**

</div>