# Moderator
<!-- {docsify-ignore} -->
Moderator  is created by the Super Administrator or Account administrator. Moderator can be mapped to "n" number of Accounts and one Account can have multiple moderators. So, moderator can view all the projects and tasks information mapped to the respective moderator account.
Moderators are involved in the workflow to review the data submitted by the field users.
											

<p align="center"> <img width="1000" height="500" src="media/MA.jpg"> </p>

<div align="center"> 

**`Fig` Moderator activities**

</div>

<p>Enter valid Moderator username and password and click on "Sign in".</p>

Login as [Moderator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod1.jpg"></p>

<div align="center">

**`Fig.1` Moderator login page**

</div>

<p>After login we can see the moderator page has</p>

 - Forms

 - Approvals

 - Downloads

<hr>







