﻿
# Account Administrator
<!-- {docsify-ignore} -->
Account administrator is created and mapped to specific Account by Super User, the activities which are created in a specific account are seen by the respective Account Administrator and Super Administrator.

<p align="center"> <img width="1000" height="500" src="media/AAA.jpg"> </p>

<div align="center"> 

**`Fig` Account administrator activities**

</div>

<p>Enter valid Account administrator username and password and click on Sign in.</p>

Login as [Account Administrator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa1.jpg"></p>

<div align="center">

**`Fig.1` Account Administrator login page**

</div>

- After login account administrator lands on Administrators page with Welcome message, list of below options in the menu bar:

 - `Administrators`	

 - `Users`

 - `Categories`	

 - `Devicemanagement`

 - `Templates`

 - `Forms`

 - `Projects`

 - `Tasks`

 - `Workflowmanagement`

 - `Downloads`

 - `Settings`





























