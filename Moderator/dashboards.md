The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.

Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Moderator:

Under moderator type of entity:

- Approvals

Approvals page showing no of pending tasks with details end date, start date, task name.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod77.jpg"></p>

<div align="center">

**`Fig.227` Dashboard-Approvals**

</div>

