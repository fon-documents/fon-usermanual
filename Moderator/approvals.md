﻿
Click on "approvals", opens approvals tab showing list of tasks (records level, task level) that are assigned to workflow with moderator waiting/ pending for the approval.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod31.jpg"></p>

<div align="center">

**`Fig.103` Approvals list**

</div>

### Approval flow – task level

- Moderator can accept task by clicking on "accept task" icon in approvals of the task, opens page with signature, comments fields.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod32.jpg"></p>

<div align="center">

**`Fig.104` Accept task**

</div>

- To accept task upload signature and comments and click on "update".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod33.jpg"></p>

<div align="center">

**`Fig.105` Approve task**

</div>

## Rejection flow – task level

- Moderator can reject task, to reject task click on "reject task" icon opens a page with signature and comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod34.jpg"></p>

<div align="center">

**`Fig.106` Reject task**

</div>

- Upload signature, comments and click on "update" the task rejected successfully. When the task is rejected then task moves to previous approver.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod35.jpg"></p>

<div align="center">

**`Fig.107` Reject task**

</div>

- Click on "workflow history" icon to view work flow details, status, comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod36.jpg"></p>

<div align="center">

**`Fig.108` Workflow history of task with tasklevel workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod37.jpg"></p>

<div align="center">

**`Fig.109` Workflow history**

</div>

- Click on "work assignments" to view work assignments of task/ project.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod38.jpg"></p>

<div align="center">

</div>

- Click on "submitted records" icon, to view submitted records of work assignments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod39.jpg"></p>

<div align="center">

**`Fig.110` Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod40.jpg"></p>

<div align="center">

</div>

### Export to Excel

- To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.	 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod41.jpg"></p>

<div align="center">

**`Fig.111` Export to excel**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.112` Columns selection page**

</div>

- Once after clicking on "excel" option, now all the list of fields are displayed select the green color option. The application shows a message saying, "Please check the downloaded records in downloads tab"."

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod43.jpg"></p>

<div align="center">

</div>

### Export to Email

- Select records and click on "Export to Email" option. Opens email attachment popup.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod44.jpg"></p>

<div align="center">

**`Fig.113` Export to email**

</div>

- Moderator can email the records to the Valid Email & can also send the same to another email by adding in the Email Id in CC which is optional. Select Excel/PDF and click on send.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod45.jpg"></p>

<div align="center">

</div>

- Opens columns selection page with list of all columns, selects the columns and click on ✅. it shows a successful pop-up.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.114` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod47.jpg"></p>

<div align="center">

</div>

### Export to PDF

- Moderator can select single or multiple records and then click on "PDF" option to generate the submitted data to PDF format.

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod48.jpg"></p>

<div align="center">

**`Fig.115` Export to PDF**

</div>

- Once after clicking on "PDF" option, columns selection page opens with the list of all columns, select the columns and click on ✅. The application shows a message saying, "Please check the downloaded records in downloads tab".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.116` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod50.jpg"></p>

<div align="center">

</div>

### Inserted locations

- Select the records and then click on "Inserted Locations". Locations from where the records were submitted will be displayed as a marker on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod51.jpg"></p>

<div align="center">

**`Fig.117` Inserted locations**

</div>

### Add columns

- Click on add columns, open columns selection page with list of all columns. Select the columns and click on ✅ 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod53.jpg"></p>

<div align="center">

**`Fig.118` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.119` Columns selection page**

</div>

- The selected columns information can be updated in the table of Submitted records.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod56.jpg"></p>

<div align="center">

</div>

### Preview of submitted records

- Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod57.jpg"></p>

<div align="center">

**`Fig.120` Submitted record of task with record level workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod58.jpg"></p>

<div align="center">

**`Fig.121` Preview of record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod59.jpg"></p>

<div align="center">

</div>
Click on "actions", shows actions like accept task, reject task, work flow history, sketching, geo tagged images for the task that is assigned with record level work flow.

Shows actions like sketching, geo tagged images also.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod60.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit sketching function moderator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.121` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.122` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.123` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.124`  Sketching**

</div>

4. Edit sketching 

o Click on "edit sketching" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.125`  Edit Sketching**

</div>

o The options are displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.126`  Edit Sketching**

</div>

o By clicking on "measuring tool", displayed the options with polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.127` Edit Sketching**

</div>

o If moderator select the "polyline sketching" option, application allows moderator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.128` Draw sketching by polyline**

</div>

o Click on the "finish" option so that sketching was end and the polyline is placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.129` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.130` Line sketching**

</div>

o Click on the "tick" icon to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.131` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.132` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching saved successfully and displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.133` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps moderator can be sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures moderator can be sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.134` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.135` Polygon sketching**

</div>

o Moderator can place a marker on the map by using "marker" option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.136` Marker sketching**

</div>

6. Delete sketching

o Moderator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.137` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.138` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.139` Delete sketching**

</div>

o Click on the "tick" mark option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.140` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.141` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.142` Update sketching**

</div>

7. Edit sketching

o Moderator click on the "edit sketching" icon on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.143`  Edit Sketching**

</div>

o Moderator click on the "measuring tool" option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.144`  Measuring tool**

</div>

o Moderator click on the "edit layers" option. Moderator can see sketching data are enabled into edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.145`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.146`  Sketching in edit mode**

</div>

o Moderator can edit by draging handles on the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.147`  Drag Handles or marker**

</div>


o After changes are done moderator click on the "save" button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.148` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.149`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.150`  Update Button**

</div>

o Moderator gets "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.151`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen" view option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.152` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.153` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Moderator can be add and edit  sketching properties information for sketching objects on the map

2. Moderator click on the "edit sketching" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.154`  Edit Sketching**

</div>

3. Moderator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.155`  Sketching Properties Pop-Up menu**

</div>

4. Moderator by clicking on "edit" icon in skecting properties, administartor able to edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.156`  Sketching Properties**

</div>

5. By clicking on "edit" option, form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.157` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.158` Edit Sketching Properties**

</div>

7. After successfully added the properties to sketching,gets displayed "toast message".  

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.159` Edit Sketching Properties**

</div>

### Search on map 

o Click on "search icon" on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.160` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.161` Search on map**

</div>

### Measuring Tool

Measuring tool is used to measure sketchings on map like distance, area of line, circle, rectangle, polygon.

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.jpg"></p>

<div align="center">

**`Fig.162` submitted records table**

</div>

2. Click On submitted record by field user.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.jpg"></p>

<div align="center">

**`Fig.163` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.jpg"></p>

<div align="center">

**`Fig.164` submitted record preview**

</div>

3. Click on "ellipse" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.jpg"></p>

<div align="center">

**`Fig.165` Click on ellipse button**

</div>

4. Click on "sketchings" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.jpg"></p>

<div align="center">

**`Fig.166` Click on sketching**

</div>

5. View sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.jpg"></p>

<div align="center">

**`Fig.167` View Sketching on map**

</div>

6. Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.jpg"> </p>

<div align="center"> 

**`Fig.168` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.

 - Draw a Ploygon.

 - Draw a rectangle.

 - Draw a marker.

 - Edit sketching.

 - Delete sketching.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.jpg"> </p>

<div align="center"> 

**`Fig.169` Draw tool**

</div>

7. Click on "Draw a polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.jpg"> </p>

<div align="center"> 

**`Fig.170` click on draw a polyline**

</div>

8. By click on "Polyline" icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.jpg"> </p>

<div align="center"> 

**`Fig.171` Drawn a poly line**

</div>

9. Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.jpg"> </p>

<div align="center"> 

**`Fig.172` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.jpg"> </p>

<div align="center"> 

**`Fig.173` Length of distance**

</div>

11. Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.jpg"> </p>

<div align="center"> 

**`Fig.174` Click on polygon icon**

</div>

12. To draw a polygon on map then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.jpg"> </p>

<div align="center"> 

**`Fig.175` Drawn polygon on map**

</div>

13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.jpg"> </p>

<div align="center"> 

**`Fig.176` Area of polygon**

</div>

14. Click on draw a "Rectangle" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.jpg"> </p>

<div align="center"> 

**`Fig.177` Click on draw a rectangle**

</div>

15. To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.jpg"> </p>

<div align="center"> 

**`Fig.178` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.jpg"> </p>

<div align="center"> 

**`Fig.179` Area of rectangle**

</div>

17. Click on "Marker" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.jpg"> </p>

<div align="center"> 

**`Fig.180` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.jpg"> </p>

<div align="center"> 

**`Fig.181` Marker on map**

</div>

19. By click on "edit" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.jpg"> </p>

<div align="center"> 

**`Fig.182` Click on Edit icon **

</div>

20. Moderator can see drawn sketchings (polyline, polygon, Rectangle, Marker) are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.jpg"> </p>

<div align="center"> 

**`Fig.183` sketchings are in editable mode**

</div>

21. After edit sketchings click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.jpg"> </p>

<div align="center"> 

**`Fig.184` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.jpg"> </p>

<div align="center"> 

**`Fig.185` saved sketchings**

</div>

22. Click on "delete" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.jpg"> </p>

<div align="center"> 

**`Fig.186` Click on delete icon**

</div>

23. Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.jpg"> </p>

<div align="center"> 

**`Fig.187` Click on clearAll**

</div>

24. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.jpg"> </p>

<div align="center"> 

**`Fig.188` Clear All drawed sketchings on map**

</div>

25. Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.jpg"> </p>

<div align="center"> 

**`Fig.189` Click on delete icon**

</div>

26. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.jpg"> </p>

<div align="center"> 

**`Fig.190` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.jpg"> </p>

<div align="center"> 

**`Fig.191` Deleted drawn rectangle**

</div>

### Approval flow- record level

Click on "accept task", opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task accepted successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod61.jpg"></p>

<div align="center">

**`Fig.192` Accept record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod62.jpg"></p>

<div align="center">

**`Fig.193` Approved record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod63.jpg"></p>

<div align="center">

</div>

### Rejection flow- record level

Click on "reject task", opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task rejected successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod64.jpg"></p>

<div align="center">

**`Fig.194` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod65.jpg"></p>

<div align="center">

**`Fig.195` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod66.jpg"></p>

<div align="center">

</div>

Click on "work flow history", opens page click on view details to view the work flow details, comments, status.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod67.jpg"></p>

<div align="center">

**`Fig.196` Workflow history for record level**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod68.jpg"></p>

<div align="center">

</div>

Click on "sketching", to see the sketching done by field user in map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod69.jpg"></p>

<div align="center">

**`Fig.197` Sketching**

</div>

### Measuring Tool

Measuring tool is used to measure sketchings on map like distance, area of line, circle, rectangle, polygon.

1. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.jpg"></p>

<div align="center">

**`Fig.198` Sketching on map**

</div>

2. Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.jpg"> </p>

<div align="center"> 

**`Fig.199` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.

 - Draw a Ploygon.

 - Draw a rectangle.

 - Draw a marker.

 - Edit sketching.

 - Delete sketching.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.jpg"> </p>

<div align="center"> 

**`Fig.200` Draw tool**

</div>

3. Click on Draw a "polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.jpg"> </p>

<div align="center"> 

**`Fig.201` click on draw a polyline**

</div>

4. By click on Polyline icon moderator can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.jpg"> </p>

<div align="center"> 

**`Fig.202` Drawn a poly line**

</div>

5. Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.jpg"> </p>

<div align="center"> 

**`Fig.203` Click on Finish Button**

</div>

6. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.jpg"> </p>

<div align="center"> 

**`Fig.204` Length of distance**

</div>

7. Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.jpg"> </p>

<div align="center"> 

**`Fig.205` Click on polygon icon**

</div>

8. To draw a polygon then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.jpg"> </p>

<div align="center"> 

**`Fig.206` Drawn polygon on map**

</div>

9. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.jpg"> </p>

<div align="center"> 

**`Fig.207` Area of polygon**

</div>

10. Click on draw a "Rectangle" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.jpg"> </p>

<div align="center"> 

**`Fig.208` Click on draw a rectangle**

</div>
11. To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.jpg"> </p>

<div align="center"> 

**`Fig.209` Drawn rectangle on map**

</div>

12. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.jpg"> </p>

<div align="center"> 

**`Fig.210` Area of rectangle**

</div>

13. Click on Marker icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.jpg"> </p>

<div align="center"> 

**`Fig.211` click on marker icon**

</div>

14. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.jpg"> </p>

<div align="center"> 

**`Fig.212` Marker on map**

</div>

15. By click on "edit" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.jpg"> </p>

<div align="center"> 

**`Fig.213` Click on Edit icon**

</div>

16. We can see drawn sketchings (polyline, polygon, Rectangle, Marker) are changed to editable mode.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.jpg"> </p>

<div align="center"> 

**`Fig.214` sketchings are in editable mode**

</div>

17. After edit sketchings click on "save" option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.jpg"> </p>

<div align="center"> 

**`Fig.215` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.jpg"> </p>

<div align="center"> 

**`Fig.216` saved sketchings**

</div>

18. Click on delete icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.jpg"> </p>

<div align="center"> 

**`Fig.217` Click on "delete" icon**

</div>

19. Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.jpg"> </p>

<div align="center"> 

**`Fig.218` Click on clearAll**

</div>

20. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.jpg"> </p>

<div align="center"> 

**`Fig.219` Clear All drawed sketchings on map**

</div>

21. Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.jpg"> </p>

<div align="center"> 

**`Fig.220` Click on delete icon**

</div>

22. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.jpg"> </p>

<div align="center"> 

**`Fig.221` Click on drawn rectangle**

</div>

23. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.jpg"> </p>

<div align="center"> 

**`Fig.222` Deleted drawn rectangle**

</div>


<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod122.jpg"></p>

<div align="center">

</div>

24. Click on "geo tagged images", to see the images that are geo tagged by field user on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod71.jpg"></p>

<div align="center">

**`Fig.223` Geo taggged images**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod72.jpg"></p>

<div align="center">

</div>

25. To edit the submitted records, enable the toggle to edit and make changes and click on "update". Shows message updated successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod73.jpg"></p>

<div align="center">

**`Fig.224` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod74.jpg"></p>

<div align="center">

**`Fig.225` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod75.jpg"></p>

<div align="center">

</div>