﻿
Moderator can have access to forms only which has records waiting for moderator's approval. Moderator can approve or reject the records.

1. Please click on "Forms" tab, moderator can see the list of Forms available.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod2.jpg"></p>

<div align="center">

**`Fig.2` Forms page**

</div>

2. Search forms, shows the forms with the searched input if any.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod3.jpg"></p>

<div align="center">

**`Fig.3` Search forms**

</div>

3. Filter forms, click on "filter" button, select type All/public/private, shows forms based on the selection. 

<p align = "center"><img width = "1000 "height ="450" src = "media/GA/ga29.jpg"></p>

<div align="center">

**`Fig.4` Filter forms**

</div>

4. GroupBy category, click on "group by category" shows categories and forms under each category. 

<p align = "center"><img width = "1000 "height ="450" src = "media/GA/ga30.jpg"></p>

<div align="center">

**`Fig.5` Group By category-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod6.jpg"></p>

<div align="center">

**`Fig.6` Group By categories-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod7.jpg"></p>

<div align="center">

**`Fig.7` Forms under each category**

</div>

### Form preview

1. To preview the Form, go to the form and hover on the form, moderator can see preview icon. Click on "preview" icon a popup opens with all the widgets in the form. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod8.jpg"></p>

<div align="center">

**`Fig.8` Preview of form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod9.jpg"></p>

<div align="center">

**`Fig.9` Preview of form**

</div>

### Form Export to Excel

1. Moderator can Export the Form to Excel to get all the widgets of the form available in the form of excel. 

2. Moderator can use fill this excel and upload in Work Assignments to Field user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod10.jpg"></p>

<div align="center">

**`Fig.10` Export to Excel-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod11.jpg"></p>


### Submitted records- Form

1. Hover on the form click on "submitted records" icon . Shows records within 24 hrs. 

2. Click on "filter" button, opens a filter records popup. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod12.jpg"></p>

<div align="center">

**`Fig.11` Filter submitted records of forms**

</div>

3. Select from date, to date and select Field users, click on "get data" button. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod13.jpg"></p>

<div align="center">

**`Fig.12` Filter records**

</div>

4. Export to Excel, please select records and click on "export to excel" button, opens columns selections page in the right side. Select the columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod15.jpg"></p>

<div align="center">

**`Fig.14` Export to excel-Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.15` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod17.jpg"></p>

<div align="center">

</div>

5. Export to mail, please select records and click on "export to mail" button, opens a popup with name email attachment with fields To, CC, select PDF or excel and click on send. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod18.jpg"></p>

<div align="center">

**`Fig.16` Export to Email**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod19.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod20.jpg"></p>

<div align="center">

**`Fig.17` Email attachment form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod21.jpg"></p>

<div align="center">

</div>

6. Export to PDF, please select records and click on export to "PDF" button, opens columns selection page with list of columns. Select columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.jpg"></p>

<div align="center">

**`Fig.18` Export to PDF**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod23.jpg"></p>

<div align="center">

**`Fig.19` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod24.jpg"></p>

<div align="center">

</div>

7. Inserted locations, please select record and click on "inserted location" button, map opens and shows the inserted location of the selected record. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod25.jpg"></p>

<div align="center">

**`Fig.20` Inserted locations**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod26.jpg"></p>

<div align="center">

</div>

8. Add columns, click on "add columns" button, open columns selection page on the right side with list of columns. Select required columns and click on ✅. Selected columns are shown in the records table.  

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod27.jpg"></p>

<div align="center">

**`Fig.21` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod28.jpg"></p>

<div align="center">

**`Fig.22` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod29.jpg"></p>

<div align="center">

**`Fig.23` Add columns**

</div>


!> Can export upto 1000 records to Excel 

!> Can export upto 10 records to mail/PDF 

### Preview of submitted records

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.23` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.24` Preview of submitted record**

</div>

2. Click on "actions", shows actions options like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by field user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.25` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.26`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by field user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.27`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.28` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.29`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit sketching function moderator can see sketch objects on the map. And edit sketching functionality is explained in the below steps.

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by field user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.30` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.31` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.32` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.33`  Sketching**

</div>

4. Edit sketching 

o Click on "edit sketching" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.34`  Edit Sketching**

</div>

o The options will be displayed and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.35`  Edit Sketching**

</div>

o By clicking on "measuring tool", options get display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.36` Edit Sketching**

</div>

o If moderator select the polyline sketching option, application allows moderator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.37` Draw sketching by polyline**

</div>

o Click on the "finish" option so that sketching was end and the polyline is placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.38` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.39` Line sketching**

</div>

o Click on the "tick icon" to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.40` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.41` Save sketching**

</div>

o By clicking on "yes" in pop-up menu the sketching is saved successfully and displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.42` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps mderator can sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures moderator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.43` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.44` Polygon sketching**

</div>

o Moderator can place a marker on the map by using marker option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.45` Marker sketching**

</div>

6. Delete sketching

o Moderator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.46` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.47` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.48` Delete sketching**

</div>

o Click on the "tick mark" option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.49` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.50` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.51` Update sketching**

</div>

7. Edit sketching

o Moderator click on the "edit sketching" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.52`  Edit Sketching**

</div>

o Moderator click on the "measuring tool" option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.53`  Measuring tool**

</div>

o Moderator click on the "edit option". Moderator can see sketching data are enabled into edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.54`  Edit sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.55`  Sketching in edit mode**

</div>

o Moderator can edit by dragging handles on the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.56`  Drag Handles or marker**

</div>

o After changes are done click on the "save" button, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.57` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.58`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.59`  Update Button**

</div>

o Moderator  gets a "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.60`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view" option, the map opens in full screen

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.61` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.62` Full screen view sketching**

</div>

### Add/Edit sketching properties in the sketching

1. Moderator can add and edit  sketching properties information for sketching objects on the map

2. Moderator click on the "edit sketching" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.63`  Edit Sketching**

</div>

3. Moderator click on the "sketching object", pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.64`  Sketching Properties Pop-Up menu**

</div>

4. Moderator by clicking on "edit" button in skecting properties, administartor can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.65`  Sketching Properties **

</div>

5. By clicking on "edit" option, form opens on the right side with all editable fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.66` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.67` Edit Sketching Properties**

</div>

7. After successfully added the properties to sketching,displayed "toast message".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.68` Edit Sketching Properties**

</div>

### Search on map 

o Click on "search" icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.69` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.70` Search on map**

</div>


### Measuring Tool

Measuring tool is used to measure sketchings on map like distance, area of line, circle, rectangle, polygon.

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.jpg"></p>

<div align="center">

**`Fig.71` submitted records table**

</div>

2. Click On submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.jpg"></p>

<div align="center">

**`Fig.72` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.jpg"></p>

<div align="center">

**`Fig.73` submitted record preview**

</div>

3. Click on "ellipse" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.jpg"></p>

<div align="center">

**`Fig.74` Click on ellipse button**

</div>

4. Click on "sketchings" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.jpg"></p>

<div align="center">

**`Fig.75` Click on sketching**

</div>

5. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.jpg"></p>

<div align="center">

**`Fig.76` View Sketching on map**

</div>

6. Click on "draw tool" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.jpg"> </p>

<div align="center"> 

**`Fig.77` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.

 - Draw a Ploygon.

 - Draw a rectangle.

 - Draw a marker.

 - Edit sketching.

 - Delete sketching.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.jpg"> </p>

<div align="center"> 

**`Fig.78` Draw tool**

</div>

7. Click on "Draw a polyline" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.jpg"> </p>

<div align="center"> 

**`Fig.79` click on draw a polyline**

</div>

8. By click on "Polyline" icon we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.jpg"> </p>

<div align="center"> 

**`Fig.80` Drawn a poly line**

</div>

9. Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.jpg"> </p>

<div align="center"> 

**`Fig.81` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.jpg"> </p>

<div align="center"> 

**`Fig.82` Length of distance**

</div>

11. Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.jpg"> </p>

<div align="center"> 

**`Fig.83` Click on polygon icon**

</div>

12. To draw a polygon then click on "finish" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.jpg"> </p>

<div align="center"> 

**`Fig.84` Drawn polygon on map**

</div>


13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.jpg"> </p>

<div align="center"> 

**`Fig.85` Area of polygon**

</div>

14. Click on draw a Rectangle icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.jpg"> </p>

<div align="center"> 

**`Fig.86` Click on draw a rectangle**

</div>
15. Draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.jpg"> </p>

<div align="center"> 

**`Fig.87` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.jpg"> </p>

<div align="center"> 

**`Fig.88` Area of rectangle**

</div>

17. Click on "Marker" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.jpg"> </p>

<div align="center"> 

**`Fig.89` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.jpg"> </p>

<div align="center"> 

**`Fig.90` Marker on map**

</div>


19. By click on "edit sketching" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.jpg"> </p>

<div align="center"> 

**`Fig.91` Click on Edit sketching icon **

</div>

20. We can see drawn sketchings(polyline, polygon, Rectangle, Marker) are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.jpg"> </p>

<div align="center"> 

**`Fig.92` sketchings are in editable mode**

</div>

21. After edit sketchings click on save option.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.jpg"> </p>

<div align="center"> 

**`Fig.93` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.jpg"> </p>

<div align="center"> 

**`Fig.94` saved sketchings**

</div>

22. Click on "delete" icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.jpg"> </p>

<div align="center"> 

**`Fig.95` Click on delete icon**

</div>

23. Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.jpg"> </p>

<div align="center"> 

**`Fig.96` Click on clearAll**

</div>

24. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.jpg"> </p>

<div align="center"> 

**`Fig.97` Clear All drawed sketchings on map**

</div>

25. Click on "Delete" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.jpg"> </p>

<div align="center"> 

**`Fig.98` Click on delete icon**

</div>

26. Click on drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.jpg"> </p>

<div align="center"> 

**`Fig.99` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.jpg"> </p>

<div align="center"> 

**`Fig.100` Deleted drawn rectangle**

</div>

### Info-Form

To see information of a Form hover on the Form, we can see "info" icon. Click on the "info" icon. 

1. Opens a popup with information of the form in a table:

 -	Form Name

 -	Created by

 -	Creation date

 -	No of assignments

 -	No of cases collected

 -	Last form modified date

 -	Last downloaded date

 -	Last downloaded by

 -	No of users downloaded

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod29.jpg"></p>

<div align="center">

**`Fig.101` Info-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod30.jpg"></p>

<div align="center">

**`Fig.102` Info-form**

</div>