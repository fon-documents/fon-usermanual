The "Downloads" tab provides the feasibility to download the record reports, that is both excel and PDF reports.

It has the details like file name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.

The files under "Downloads" tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod76.jpg"></p>

<div align="center">

**`Fig.226` Downloads**

</div>
