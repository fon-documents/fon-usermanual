


We have two types of approval cycles for workflow in FieldOn.

 - Task level work flow

 - Record level work flow


#### Task Level Workflow:

The "Task Level Workflow" is the one assigned to the "Tasks", the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

Here we take an example of Task Level Workflow on a task. (Note: We have used same workflow as used in task level workflow). 

 - After field user has submitted the assigned records, the workflow gets triggered and the task goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on "Submitted Record" icon.
 
 - Basically, work flow starts from group admin to moderator. After group admin has approved the task after checking the submitted records of that task's work assignment, moderator can approve or reject that task in approvals tab. 

 - Now when "Moderator" logs in, can find the task in approvals tab, showing as waiting for approval. 
 
 - After moderator approves the records of task's work assignment, next approver in the work flow will get access to accept or reject records.

 - If the first person of the work flow rejects the task, the task was reassigned to the field user.

#### Record Level Workflow:

The "Record Level Workflow" is the one assigned on the "Tasks", the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

Here we take an example of Record Level Workflow on a task.

 - Whenever any record is submitted for this task's work assignment, the workflow gets triggered and the record goes to the person at the first level of the workflow. Can find the records submitted for work assignment of the task by clicking on "Submitted Record" icon.

 - Basically, work flow starts from group admin to moderator. After group admin has approved the records of a task's work assignment which is assigned to record level work flow, moderator can be approve or reject records of that task's work assignment in approvals tab.

 - Now when "Moderator" logs in, can find the record in RED color in forms records, waiting for approval. 

 - After moderator approves the records of tasks work assignment, next approver in the work flow will get access to accept or reject records.

 - If the record is rejected by first approval in the work flow, record was reassigned to the field user.