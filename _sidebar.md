
* Introduction
    * [Getting Started](/basic.md)

* Super Administrator
    *  [Super Administrator Activities](/SuperAdministrator.md)
       * [Accounts](/SuperAdministrator/accounts.md)
       * [Administrators](/SuperAdministrator/administrators.md)
       * [Users](/SuperAdministrator/users.md)
       * [Categories](/SuperAdministrator/categories.md)
       * [Device Management](/SuperAdministrator/deviceManagement.md)
       * [Templates](/SuperAdministrator/templates.md)
       * [Forms](/SuperAdministrator/forms.md)
       * [Downloads](/SuperAdministrator/downloads.md)
       * [Settings](/SuperAdministrator/settings.md)
       * [Dashboards](/SuperAdministrator/dashboards.md)
       

* Account Administrator
     * [Account Administrator Activities](/AccountAdministrator.md)    
        * [Administrators](/AccountAdministrator/administrators.md)  
        * [Users](/AccountAdministrator/users.md) 
        * [Categories](/AccountAdministrator/categories.md)
        * [Device Management](/AccountAdministrator/deviceManagement.md)
        * [Templates](/AccountAdministrator/templates.md)
        * [Forms](/AccountAdministrator/forms.md)
        * [Projects](/AccountAdministrator/projects.md)
        * [Tasks](/AccountAdministrator/tasks.md)
        * [Workflow Management](/AccountAdministrator/workflowManagement.md)
        * [Workflow Approval Cycle](/AccountAdministrator/workflowApprovalCycle.md)
        * [Downloads](/AccountAdministrator/downloads.md)
        * [Settings](/AccountAdministrator/settings.md)
   
* Group Administrator
     * [Group Administrator Activities](/GroupAdministrator.md)       
        * [Users](/GroupAdministrator/users.md) 
        * [Forms](/GroupAdministrator/forms.md)
        * [Projects](/GroupAdministrator/projects.md)
        * [Tasks](/GroupAdministrator/tasks.md)
        * [Workflow Approval Cycle](/GroupAdministrator/workflowApprovalCycle.md)
        * [Downloads](/GroupAdministrator/downloads.md)
        * [Dashboards](/GroupAdministrator/dashboards.md)


   
* Moderator
     * [Moderator Activities](/Moderator.md)       
        * [Forms](/Moderator/forms.md)
        * [Approvals](/Moderator/Approvals.md)
        * [Workflow Approval Cycle](/Moderator/workflowApprovalCycle.md)
        * [Downloads](/Moderator/downloads.md)
        * [Dashboards](/Moderator/dashboards.md)

        
     
       
*Field User
    * [Field User Activities](/fieldUser.md) 

 
