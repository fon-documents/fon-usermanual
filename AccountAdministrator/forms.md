﻿

<p>Account administrator can create forms  based on their requirements using the available widgets and the field user created by account administrator will have access to submit data.
These forms can be used to create work assignments under tasks/project tasks with a defined work flow process between group administrators and moderators to review the data submitted by field user.</p>

Account administrator can create, edit, search, preview, delete form, export form to excel, share link, view form info, version history, business rules.

1.	When clicked on Forms tab, we can see the list of Forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa79.jpg"></p>

<div align="center">

**`Fig.81` List of forms**

</div> 

2.	Account Administrator can create Forms. 

### Form Creation

1.	On the right-hand top corner, we can see ellipse. 

2.	Click on ellipse, click on "create" button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa86.jpg"></p>

<div align="center">

**`Fig.82` Create forms**

</div>

3.	Create form opened with name, category, Form Type, Description and Design from Excel

 a.	`Private:` If private selected, then the form  visible to only selected Users

 b.	`Public:` If public selected, then the form visible to all Users of that Account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa87.jpg"></p>

<div align="center">

**`Fig.83` Create form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa88.jpg"></p>

<div align="center">

**`Fig.84` Create form**

</div>

4.	After clicking of "Go" button. It  open 'Form Builder' where we can select the widgets and create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa89.jpg"></p>

<div align="center">

**`Fig.85` Form widgets**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa90.jpg"></p>

<div align="center">

**`Fig.86` Media widgets and Addon widgets**

</div>

5.	Drag and drop the widgets to the pallate to create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa91.jpg"></p>

<div align="center">

</div>

6.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.

7.	After saving of the widget, save the Form by clicking the "save" button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa92.jpg"></p>

<div align="center">

</div>

8.	After clicking of the save button we  get a popup with a drop down where we have to select the Display fields and save the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa93.jpg"></p>

<div align="center">

**`Fig.87` Create form**

</div>

!> Duplicate names not allowed.

### Form creation using excel

1.	Create form using excel template by uploading excel in Form creation page.

2.	Download 'creation Template' from the creation page 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa95.jpg"></p>

<div align="center">

**`Fig.88` Form creation using excel**

</div>

3.	After filling the Excel, we need to upload the excel by clicking Choose File button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa96.jpg"></p>

<div align="center">

</div>

4.	After uploading click on "Go" button, we can see the Form Builder with widgets.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa97.jpg"></p>

<div align="center">

</div>

5.	Update the details and save the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa98.jpg"></p>

<div align="center">

</div>

### Form Preview

1.	To Preview the Form, go to the form and hover on the form, we can see preview button, Click on preview button a popup  be open with all the widgets in the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa99.jpg"></p>

<div align="center">

**`Fig.89` Form preview**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa100.jpg"></p>

<div align="center">

**`Fig.90` Form preview**

</div>

### Form Edit/Update

1.	To edit a Form hover on the Form, we can see Edit button.

2.	On Click of edit button on the form we can see the existing details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa101.jpg"></p>

<div align="center">

**`Fig.91` Edit form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa102.jpg"></p>

<div align="center">

**`Fig.92` Edit form**

</div>

3.	After clicking of 'Go' button. It open Form Builder with the existing widgets and can add new widgets to the Form.

4.	We need to drag and drop the widgets to the pallate to add new widgets to Form.

5.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.

6.	After saving of the widget we need to save the Form by clicking the "save" button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa103.jpg"></p>

<div align="center">

</div>

7.	After clicking of the save button a popup with a drop down opens, select the Display fields and click on update, updates the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa104.jpg"></p>

<div align="center">

**`Fig.93` Form Update**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa105.jpg"></p>

<div align="center">

</div>

###  Form Derived Conditions

1.	To add derived conditions to a form, after clicking on edit instead of Save click on  "Save" and "Continue".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa106.jpg"></p>

<div align="center">

</div>

2.	After clicking on, we can see a side bar opened with Save, Add and Back buttons.

3.	Once click on Add we can see the dropdown to select and set a condition.

4.	Click on Set to set the condition and Apply.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa107.jpg"></p>

<div align="center">

**`Fig.94` Derived conditions of form**

</div>

5.	After that we can see the conditions in a list where we can delete and save the complete list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa108.jpg"></p>

<div align="center">

</div>

6.	Click on Save to save the form.

### Form Export to Excel

Exported excel form can be used in work assignment creation with pre-populated data. The provided data in the excel form is visible as pre-populated records to assigned field user.

1.	To export form to excel hover on the form, click on export to excel icon, downloads the form in excel.

2.	We can Export the Form to Excel to get all the widgets of the form available in the form in excel.

3.	We can use fill this excel and upload in Work Assignments to user.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa109.jpg"></p>

<div align="center">

**`Fig.95` Export to excel-Form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa110.jpg"></p>

<div align="center">

**`Fig.96` Excel file of form**

</div>

### Submitted records - form

1.	Click on submitted records icon which in on hover on the form. Shows records within 24 hrs.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa111.jpg"></p>

<div align="center">

**`Fig.97` Submitted records of form**

</div>

2.	Click on "filter" icon, opens a filter records popup.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa112.jpg"></p>

<div align="center">

**`Fig.98` Filter records**

</div>

3.	Select from date, to date and select users, click on get data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa113.jpg"></p>

<div align="center">

**`Fig.99` Filter submitted records of form**

</div>

*  Adminsitrator can export submitted Records
   *  Export to excel
   *  Export to Mail
   *  Export to PDF
   *  See on map


To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)

### Share Link - form

Form link can be accessed by anyone which can be used to share for anonymous data submission.

1.	To share a form link, we need to select the Form and hover on the form, we can see the 'Share Link' button. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa128.jpg"></p>

<div align="center">

**`Fig.100` Share link of form**

</div>

2.	After clicking share link button, we can see a popup with validity, we need to select the expiry date and click on Generate button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa129.jpg"></p>

<div align="center">

</div>

3.	After clicking of generate we can see the Generated 'Link' and expiry date. we can copy the link by clicking on copy button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa130.jpg"></p>

<div align="center">

**`Fig.101` Copy link of form**

</div>

4.	Paste the copied link in the new tab, opens the form

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa131.jpg"></p>

<div align="center">

</div>

### Info - Form

1.	To see information of a Form hover on the Form, we can see info button. Click on the info button.

2.	Opens a popup with information of the form in a table which displays below details:

 - Form name

 - Created by 

 - Creation date

 - No.of assignments

 - No.of cases collected

 - last form modified date

 - last downloaded date

 - last downloaded by

 - No.of users downloaded


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa132.jpg"></p>

<div align="center">

**`Fig.102` Info-form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa133.jpg"></p>

<div align="center">

**`Fig.103` Info-form**

</div>

### Version History - Form

1.	We can view list of changes done on the form after creation as Version History.

2.	To view the version history of a Form, hover on the form and click Version History button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa134.jpg"></p>

<div align="center">

**`Fig.104` Version history-form**

</div>

3.	After clicking on that we can view the list of versions of that form.

4.	Click on the 'Info' icon to view the changes done on that form in that version.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa135.jpg"></p>

<div align="center">

</div>

5.	A popup open with Added, Updated and Deleted categories.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa136.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa137.jpg"></p>

<div align="center">

**`Fig.105` Version history details**

</div>

### Business rules - Form

<p>Account administrator can create business rule for forms, when field user submits data as per the set business rule, red flags get regerated for those fields.
The red flags are visible in the submitted record preview to all the associated administrators. Basically, these business rules are created to identify the flaws in the data easily while reviewing the field user submitted data.</p>

1. On Clicking on Forms on the left-hand side menu bar, we can see list of forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa307.jpg"></p>

<div align="center">

**`Fig.106` List of forms**

</div>

2. To business rules, select the form and hover on it we can see business rules option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa308.jpg"></p>

<div align="center">

**`Fig.107`Set rules forms**

</div>

3. Select the business rules option, red flag rules  be opened with already created rule if any or show No rules defined message and Add new rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa309.jpg"></p>

<div align="center">

**`Fig.108` Add new rule**

</div>

4. Clik on Add new rule option, blank row  be added with 'Filed', 'Rule' and dropdowns, 'Value' field and save rule, delete rule option in Action.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa310.jpg"></p>

<div align="center">

**`Fig.109`Add new rule**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa311.jpg"></p>

<div align="center">

**`Fig.110` Add new rule**

</div>

#### Set rule for Text Box

1. On selection Field of type Text Box from field dropdown, in rule dropdown Equalto, Notequal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa312.jpg"></p>

<div align="center">

**`Fig.111` Set rules for Text box field**

</div>

2. On selection of rule as Equaltp/NotEqual to, Account administrator  be  enter text in valid field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa313.jpg"></p>

<div align="center">

**`Fig.112` Set rules for text box field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa314.jpg"></p>

<div align="center">

**`Fig.113` Set rules for Text box field**

</div>

3. On selection Rule as Blank, Value field is disabled

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa315.jpg"</p>

<div align="center">

**`Fig.114` Set rules for Text box field**

</div>

4. After choosing field, Rule and Value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa316.jpg"></p>

<div align="center">

**`Fig.115` Set rules for Text box field**

</div>

5. Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa317.jpg"></p>

<div align="center">

**`Fig.116` Saved rules for Text box field**

</div>

6. In redflag rules page, Rule  be shown in the list.

#### Set rule for Text Area field

1.	On Selecting Field of type Text Area from Field dropdown, in Rule Dropdown Equal to, Not Equal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa318.jpg"></p>

<div align="center">

**`Fig.117` Set rule for Text area field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa319.jpg"></p>

<div align="center">

**`Fig.118`   Set rule for Text area field**                         

</div>

2.	On selecting Rule as Equal to/ Not Equal to, Account administrator  enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa320.jpg"></p>

<div align="center">

**`Fig.119`  Save rule for area field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

4.	After choosing field, rule and value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa321.jpg"></p>

<div align="center">

**`Fig.120` Save rule for area field**

</div>

5.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa322.jpg"></p>

<div align="center">

**`Fig.121` Saved rule for area field**

</div>

6.In red flag rules page, Rule  be shown in the list.

#### Set rule for Number field

1.	On Selecting Field of type Number from Field dropdown, in Rule dropdown less than, greater than, Equal to, Blank options are visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa323.jpg"></p>

<div align="center">

**`Fig.122` Set rule for Number field**

</div>

2.	On selecting Rule as Greater than/less than/ Equal to, enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa324.jpg"></p>

<div align="center">

**`Fig.123` Set rule for Number field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa325.jpg"></p>

<div align="center">

**`Fig.124` Save rule for number field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa326.jpg"></p>

<div align="center">

**`Fig.125` Saved rule for number field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Dropdown field

1.	On Selecting Field of type Dropdown from Field dropdown, in Rule dropdown contains, not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa327.jpg"></p>

<div align="center">

**`Fig.126` Set rule for Dropdown field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator view and select options of the selected dropdown in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa328.jpg"></p>

<div align="center">

**`Fig.127` Set rule for Dropdown field**

</div>

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa329.jpg"></p>

<div align="center">

**`Fig.128` Save rule for Dropdown field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa330.jpg"></p>

<div align="center">

**`Fig.129` Saved rule for Dropdown field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Radio field

1.	On Selecting Field of type Radio from Field dropdown, in Rule dropdown contains, not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa331.jpg"></p>

<div align="center">

**`Fig.130` Set rule for Radio field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator can view and select options of the selected radio field in value field.

3.	On selecting Rule as Blank, value field is disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa332.jpg"></p>

<div align="center">

**`Fig.131` Set rule for Radio field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa333.jpg"></p>

<div align="center">

**`Fig.132` Saved rule for Radio field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Check box field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown contains, Not contains, Blank options is visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa334.jpg"></p>

<div align="center">

**`Fig.133` Set rule for Check box field**

</div>

2.	On selecting Rule as contains/not contains, Account administrator can view and select options of the selected radio in value field.

3.	On selecting Rule as Blank, value field is disabled. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa335.jpg"></p>

<div align="center">

**`Fig.134` Set rule for Check box field**

</div>

4.	After choosing field, rule and value click on save rule option. 

5.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa336.jpg"></p>

<div align="center">

**`Fig.135` Saved rule for check box field**

</div>

6.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Calendar field

1.	On Selecting Field of type calendar from Field dropdown, in Rule dropdown greater than, less than options  be visible.

2.	On selecting Rule as greater than/less than, Account administrator  can view and select date in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa337.jpg"></p>

<div align="center">

**`Fig.136` Set rule for calendar field**

</div>

3.	After choosing field, rule and value click on save rule option. 

4.	Application shows toast message as "Rule saved successfully".

5.	In red flag rules page, Rule  be shown in the list.

#### Set rule for Calculator field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown Greater than, less than, equal to, Blank options visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa338.jpg"></p>

<div align="center">

**`Fig.137` Set rule for calculator field**

</div>

2.	On selecting Rule as greater than/less than/equal to, Account administrator can enter the number in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa339.jpg"></p>

<div align="center">

**`Fig.138` Save rule for calculator field**

</div>

3.	On selecting Rule as Blank, value field  be disabled. After choosing field, rule and value, click on save rule option.

4.	Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa340.jpg"></p>

<div align="center">

**`Fig.139` Saved rule for calculator field**

</div>

5.	In red flag rules page, Rule us shown in the list.

#### Edit rule

1.	All the rules in editable mode, make changes by selecting the field, rule and value and click on save rule icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa341.jpg"></p>

<div align="center">

**`Fig.140` Edit rule**

</div>

2.	New changes are saved successfully and shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa342.jpg"</p>

<div align="center">

**`Fig.141` Edit rule**

</div>

#### Delete rule

1.	Click on delete icon, application show confirmation action message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa343.jpg"></p>

<div align="center">

**`Fig.141` Delete rule**

</div>

2.	On clicking on yes, rule deleted successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa344.jpg"></p>

<div align="center">

**`Fig.142` Delete rule**

</div>

3.	Shows toast message as "Rule deleted successfully "and rule is removed from the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa345.jpg"></p>

<div align="center">

**`Fig.143` rule deleted**

</div>

###  Delete - form

1.	To delete 'Form' hover on the form, we can see the buttons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa138.jpg"></p>

<div align="center">

**`Fig.144`  Delete form**

</div>

2.	Once click on 'Delete' button, application shows the confirmation pop up, then select the 'yes' option.

3.	After click on 'Yes' button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa139.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa140.jpg"></p>

<div align="center">

</div>

### Search forms

1.	Search forms, shows the forms with the searched input if any. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa80.jpg"></p>

<div align="center">

**`Fig.141` Search forms**

</div> 

### Filter forms 

1.	Filter forms, click on filter icon, select type All/public/private, shows forms based on the selection.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa81.jpg"></p>

<div align="center">

**`Fig.142` Filter forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa82.jpg"></p>

<div align="center">

**`Fig.143` Private forms**

</div>

### GroupBy category

1.	GroupBy category, click on group by category shows categories and forms under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa83.jpg"></p>

<div align="center">

**`Fig.144` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa84.jpg"></p>

<div align="center">

**`Fig.145` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa85.jpg"></p>

<div align="center">

**`Fig.146` Shows forms under a category**

</div>