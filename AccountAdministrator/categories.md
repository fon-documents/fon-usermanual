
Categories are created to group the forms and templates based on its category.

Account administrator can create, edit, delete, search categories.

1.	When clicked on Categories tab, displays the list of Categories available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa44.jpg"></p>

<div align="center">

**`Fig.24` List of categories**

</div>

2.	Search categories, shows available categories with searched name on search bar and click on enter.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa45.jpg"></p>

<div align="center">

**`Fig.25` Search categories**

</div>

### Category Creation

1.	On the right-hand top corner, we can see ellipse. 

2.	On Click of "create button" in ellipse, can create new Category.

3.	A form  opened with name and description.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa46.jpg"></p>

<div align="center">

**`Fig.26` Create category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa47.jpg"></p>

<div align="center">

**`Fig.27` Create category**

</div>

4.	Fill the details and click the "create" button.

5.	After successful creation, displays a toast message.  

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa48.jpg"></p>

<div align="center">

</div>

!> Duplicate names not allowed.

### Category Edit/Update

1.	To edit Category hover on the category, buttons are displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa49.jpg"></p>

<div align="center">

**`Fig.28` Edit categories**

</div>

2.	Click on "Edit" icon, view the existing data, and make the changes In description and update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa50.jpg"></p>

<div align="center">

**`Fig.29` Edit category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa51.jpg"></p>

<div align="center">

</div>

### Category Delete

1.	To delete Category hover on the category, icons are displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa52.jpg"></p>

<div align="center">

**`Fig.30` Delete category**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the "yes" button.

3.	After click on Yes button, application displays toast message as "category deleted successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa53.jpg"></p>

<div align="center">

**`Fig.31` Delete category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa54.jpg"></p>

<div align="center">

</div>

!> Can delete category, only if it is not assigned to forms/tasks.
