

<p>The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.
Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.</p>

### Account Administrator

Under Account Administrator we have following entities:

- Account Specific

- Projects

- Tasks

- Users

- Device Management

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa297.jpg"></p>

<div align="center">

**`Fig.453`  Dashboards**

</div>

- Under the "Account Specific" entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa298.jpg"></p>

<div align="center">

**`Fig.454` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa299.jpg"></p>

<div align="center">

**`Fig.455` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa300.jpg"></p>

<div align="center">

</div>

- Under the "Projects" entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa301.jpg"></p>

<div align="center">

**`Fig.456`  Projects-Dashboards**

</div>

- Under the "Tasks" entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa302.jpg"></p>

<div align="center">

**`Fig.457` Tasks-Dashboards**

</div>

- Under the "Users" entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa303.jpg"></p>

<div align="center">

**`Fig.458` Users-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa304.jpg"></p>

<div align="center">

</div>

- Under the "Device Management" entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa305.jpg"></p>

<div align="center">

**`Fig.459` Device management-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa306.jpg"></p>

<div align="center">

</div>
