

Users are the workers who work on the assigned task/assignment and submit the data in the respective forms/assignments assigned to them from the field using mobile.

Account administrator can create users in single or bulk, edit, search, delete and reset its password.

1.	On click of Users tab, displays the list of Users available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa24.jpg"></p>

<div align="center">

**`Fig.13` Users list**

</div>

2.	Account administrator can create the users individually or can import users from excel sheet by clicking Import users option.

3.	For creating the user, click on actions/ellipse.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa25.jpg"></p>

<div align="center">

</div>

### Individual User Creation

- Click on Create User option.

- Fill the required details and click on "Create" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa26.jpg"></p>

<div align="center">

**`Fig.14` Create Users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa27.jpg"></p>

<div align="center">

**`Fig.15` User creation form**

</div>

- Once click on Create button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa28.jpg"></p>

<div align="center">

</div>

!> Duplicate emails are not allowed.

### Edit/Update User

Account administrator can edit/ update the user profiles.

  1. To edit the user profile, first select the user profile and click on Edit button.

  2. Make the changes and click on Update button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa29.jpg"></p>

<div align="center">

**`Fig.16` Edit users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa30.jpg"></p>


<div align="center">

**`Fig.17` Edit users form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa31.jpg"></p>

<div align="center">

</div>

### Delete user

1.	To delete User, select the User and click on "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa32.jpg"></p>

<div align="center">

**`Fig.18` Delete user**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the "yes" option.

3.	After click on Yes button, application shows the toast message as "user deleted successfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa33.jpg"></p>

<div align="center">

**`Fig.19` Delete users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa34.jpg"></p>

<div align="center">

</div>

!> Can delete users, only if user is not assigned to tasks.

### Reset User Password 

1.	Account administrator can reset the password of User to default password.

2.	Select the User and click on "password reset" icon.

3.	Default password  be mm@1234.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa35.jpg"></p>

<div align="center">

**`Fig.20` Reset password**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the "yes" button.

5.	After click on Yes button, application shows toast message "password reset successful". 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa36.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa37.jpg"></p>

<div align="center">

</div>

### Import Users

Import Users function defines, Account administrator can create bulk user profiles within a single moment by uploading the excel sheet. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa38.jpg"></p>

<div align="center">

**`Fig.21` Import Users**

</div>

1.	To import Users from excel sheet, first download User creation template from Import Users page and fill the user details (valid email ids). 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa39.jpg"></p>

<div align="center">

**`Fig.22` Import Users**

</div>

2.	Save the file with extension .xlsx (Only XLSX files are supported)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa40.jpg"></p>

<div align="center">

</div>

3.	Select the excel in choose file and click on show table to check the details and edit the details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa41.jpg"></p>

<div align="center">

**`Fig.23` Import Users**

</div>


4.	We can edit and delete the data by selecting Edit and Delete buttons.

5.	Click on the Upload button to upload the users.

6.	After successful upload, displays a toast messsage.

## Users activity

1. Click on activity tab in users page.

2. Application will open activity page of users with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/user activity page.jpg"></p>

<div align="center">

</div>

3. Choose From date and To date and click on filter icon.

4. Users Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/user activity.jpg"></p>

<div align="center">

</div>
