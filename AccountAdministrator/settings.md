
Click on setting icon from menu bar, application displays settings page.

Account administrator can perform some of the actions from the system. The below is the list:

- Profile

- Privileges

- Configurations

- Reference List

- Application Settings

- Layers Configuration.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/1.jpg"></p>

<div align="center">

**`Fig` Settings page**

</div>

### Profile

- Account administrator can change the user details like 'First name, Last name, Email, Phone Number, Image'.

- First, click on the profile and then edit the required details and then click on save option. The changes updated with new ones.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/2.jpg"></p>

<div align="center">

**`Fig.203`  Profile settings**

</div>

- The application  display a message saying, 'Profile Updated Successfully'.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/3.jpg"></p>

<div align="center">

</div>

### Privileges

The Account administrator can update the privileges for 'Group administrator & Moderators'.

### Group administrator Privileges

- First, select the Group administrator and then select the administrator name all the list of functionalities for the group administrator displayed.

- For the functionalities which are in view state, account administrator can change the state to Edit. Whereas for the functionalities which are in Edit state, administrator  be  change the state to View.

- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/4.jpg"></p>

<div align="center">

**`Fig.204` Previlege settings**

</div>

### Moderator Privileges

- First, select the Moderator and then select the administrator name all the list of functionalities for the Moderator displayed.

- For the functionalities which are in view state, account administrator can change the state to Edit. Whereas for the functionalities which are in Edit state, administrator  be  change the state to View.

- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/5.jpg"></p>

<div align="center">

**`Fig.205` Previllege settings**

</div>

### Configurations

- The Account administrator auto approve the devices at a single stretch by clicking on auto approve option.

- First, select the configurations and then click on auto approve devices. All the devices be auto approved for the account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/6.jpg"></p>

<div align="center">

**`Fig.206` Configuration-settings**

</div>

### Reference List

- First click on the reference list and then application ask to Upload excel data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa292.jpg"></p>

<div align="center">

**`Fig.207` Reference list creation**

</div>

- Select the required excel sheet having the drop-down fields and its values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa293.jpg"></p>

<div align="center">

**`Fig.208` Reference list creation**

</div>

- Once after selecting the valid file now click on Submit option so that the uploaded sheet updated in the system. The application display a message saying 'Reference list created successfully'.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa294.jpg"></p>

<div align="center">

</div>

- Account administrator can add or Edit the uploaded values in the system. 

### Application Settings

-  Account administrator can  update the account lock or lock interval for the users who are accessing the application by entering the invalid details for few attempts.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/10.jpg"></p>

<div align="center">

**`Fig.209` Application Settings**

</div>

- First, select the account lock, lock interval, and then click on update button. For account block enter the value and for account interval enter the value and then save the values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/11.jpg"></p>

<div align="center">

**`Fig.210` Application Settings**

</div>

### layer configuration
 
o Account administrator can perform layer configuration settings by clicking on layer configuration.

<p align="center"> <img width="1000" height="450" src="media/AA/settings page view.jpg"> </p>

<div align="center"> 

**`Fig.211` Settings page**

</div>

1. Create Layer

o Application opens layer configuration page with create layer and existing layers list.

o Click on +/create layer button.

<p align="center"> <img width="1000" height="450" src="media/AA/layers settings page.jpg"> </p>

<div align="center"> 

**`Fig.212` Layer configuration settings page**

</div>

o Add new layer form page opens with external name, internal name, layer url, layer type and create, cancel buttons.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer page.jpg"> </p>

<div align="center"> 

**`Fig.213` Add new layer page**

</div>

o Enter details, select layer type wms/wmts and click on create button.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer form.jpg"> </p>

<div align="center"> 

**`Fig.214` Add new layer page**

</div>

o Application displays toast message for layer creation.

<p align="center"> <img width="1000" height="450" src="media/AA/create layer msg.jpg"> </p>

<div align="center"> 

**`Fig.215` Toast for layer creation**

</div>

2. Edit Layer

o In layer configuration page select account from the dropdown list, shows the existing layers of the selected account.

<p align="center"> <img width="1000" height="450" src="media/AA/layer edit icon.jpg"> </p>

<div align="center"> 

**`Fig.216` Layers list**

</div>

o Click on edit layer option, opens edit layer form with layer url, layer type in editable mode and update, cancel button.

o Update the required changes and click on update button.

<p align="center"> <img width="1000" height="450" src="media/AA/layer edit.jpg"> </p>

<div align="center"> 

**`Fig.217` Update layer**

</div>

o Application displays toast message for layer update.

<p align="center"> <img width="1000" height="450" src="media/AA/layer update.jpg"> </p>

<div align="center"> 

**`Fig.218` Update layer toast**

</div>

3. Preview layer

o Click on preview layer option.

o Opens map showing layers data on map. 

<p align="center"> <img width="1000" height="450" src="media/SU/layer preview.jpg"> </p>

<div align="center"> 

**`Fig.219` Layer preview**

</div>

4.  Search on map 

o Click on search icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.220` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.221` Search on map**

</div>

5. Delete layer

o Click on delete layer icon.

<p align="center"> <img width="1000" height="450" src="media/AA/layer delete icon.jpg"> </p>

<div align="center"> 

**`Fig.222` Delete layer**

</div>

o Application displays confirm action message, click on yes.

<p align="center"> <img width="1000" height="450" src="media/AA/confirm delete.jpg"> </p>

<div align="center"> 

**`Fig.223` Delete action message**

</div>

o Application displays toast message for layer delete.

<p align="center"> <img width="1000" height="450" src="media/AA/layer delete msg.jpg"> </p>

<div align="center"> 

**`Fig.224` Toast for delete layer**

</div>