
Administrators can manage the data of an account and are involved in the workflow to review the data submitted by the field users.

1. On click of Administrators tab displays the list of Administrators available.

2. We can see all types of Administrators.

 i.	Account Administrator

 ii. Group Administrator

 iii. Moderators

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa2.jpg"></p>

<div align="center">

**`Fig.1` Administrators page**

</div>


### Administrator Create

1.	Account administrator can create the Administrator by clicking on the create button in ellipse on the top right-hand side.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa9.jpg"></p>

<div align="center">

</div>

2.	On click of "create" button, a form  open with fields first name, last name, email, phone, role, select avatar. Fill the details and click on create.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa10.jpg"></p>

<div align="center">

**`Fig.2` Administrator creation form**

</div>

3.	We have Group Administrator and Moderators.

4.	Based on the check box selection, Administrators are created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa11.jpg"></p>

<div align="center">

**`Fig.3` Group administrator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa12.jpg"></p>

<div align="center">

</div>

5.	If role is not selected, then Moderator is created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa13.jpg"></p>

<div align="center">

**`Fig.4` Moderator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa14.jpg"></p>

<div align="center">

</div>

!> No duplicate Emails allowed for same type of Administrator.

### Edit/Update Administrator

1.	To edit Administrator, select the Administrator and hover on the card we can see Edit icon.

2.	Select the Edit icon, edit administrator form opened with existing details, update the details, and click on "Update" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa15.jpg"></p>

<div align="center">

**`Fig.5` Edit administrators**

</div>

3.	After successful update, toast message is shown as "Administartor updated successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa16.jpg"></p>

<div align="center">

**`Fig.6` Edit form of administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa17.jpg"></p>

<div align="center">

</div>



### Administrator Password Reset

1. Account administrator can reset the password of Administrator to default password.

2.	Select the Administrator and click on password reset button.

3.	Default password is "mm@1234".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa21.jpg"></p>

<div align="center">

**`Fig.7` Reset password-administrators**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the yes option.

5.	After click on Yes button, application shows the toast message as "password reset successful".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa22.jpg"></p>

<div align="center">

**`Fig.8` Reset password-administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa23.jpg"></p>

<div align="center">

</div>


### Filter administrators

1. Filter the Administrators by using filter in the top right-hand side. By default, it shows all administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa3.jpg"></p>

<div align="center">

**`Fig.9` Filter Administrators**

</div>

2. Select Moderator, shows all available moderators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa4.jpg"></p>

<div align="center">

**`Fig.10` Filter moderators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa5.jpg"></p>

<div align="center">

</div>

3. Select Group Administrators, shows all available group administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa6.jpg"></p>

<div align="center">

**`Fig.11` Filter Group administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa7.jpg"></p>

<div align="center">

</div>


### Search administrators

1.  Search administrators in the search bar which is on top right-hand, shows the available administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa8.jpg"></p>

<div align="center">

**`Fig.12` Search administrators**

</div>