﻿
Account administrator can create projects by associating it to group administrator, associated group administrator can create project tasks and work assignments assigning to field users.

1.	Click on projects icon, opens projects page showing list of projects.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa141.jpg"></p>

<div align="center">

**`Fig.147`  List of projects**

</div>

2.	Search projects, Shows projects available with the searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa142.jpg"></p>

<div align="center">

**`Fig.148`  Search projects**

</div>

3.	Grid view, to view projects as grids click on the icon in the right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa143.jpg"></p>

<div align="center">

**`Fig.149` Grid view of projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa144.jpg"></p>

<div align="center">

</div>

### Project Creation

Projects are defined as the Account administrator can create Projects and assign to Group administrator, where Group administrator creates certain project tasks and assign work assignment to group of users along with some predefined data.

1.	For creating Projects first go to "Projects" tab and click on ellipse, click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa145.jpg"></p>

<div align="center">

**`Fig.150` Create projects**

</div>

2.	After clicking on ‘Create’ button, now enter the project details, like ‘Name, Description, Category, Group Admin, start date & End date’ and then click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa146.jpg"></p>

<div align="center">

</div>

3.	The application  display a message saying, ‘Project created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa147.jpg"></p>

<div align="center">

</div>

### Edit Project

Account administrator can edit/update the created Project (if required) by clicking on ‘Edit’ option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa148.jpg"></p>

<div align="center">

**`Fig.151`  Edit project**

</div>

- The details like ‘Description, End date’  available for updating.

- After updating the details click on ‘Update button’ to save the changes.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa149.jpg"></p>

<div align="center">

**`Fig.157` Create projects**

</div>

- Once after clicking on the update button, the application displays a toast message as "Project updated successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa150.jpg"></p>

<div align="center">

</div>

### Delete Project

To delete the Project, select the Project and click on ‘Delete’ button. Account administrator can delete the project only if no project task is assigned.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa151.jpg"></p>

<div align="center">

**`Fig.158`  Delete projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa152.jpg"></p>

<div align="center">

</div>

- If any task is associated with the Project, then Account administrator cannot delete.

- Toast message gets displayed as "project deleted successfully"

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa153.jpg"></p>

<div align="center">

</div>

### Project Task

 - The project task within the project created by Group administrator assigned to the project. Workflow of this functionality be same as Task.

 - Click on project tasks, shows list of projects tasks under the project which are created by assigned Group administrator.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa154.jpg"></p>

<div align="center">

**`Fig.159`  Project tasks**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa155.jpg"></p>

<div align="center">

**`Fig.160` List of project tasks**

</div>

- Search project tasks, shows projects tasks with searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa156.jpg"></p>

<div align="center">

**`Fig.161` Search project tasks**

</div>

- Preview of project tasks, shows existing details of project tasks as read only fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa157.jpg"></p>

<div align="center">

**`Fig.162` Preview of project tasks**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa158.jpg"></p>

<div align="center">

</div>

- Work assignments, click on work assignments icon in project task page, shows list of work assignments under the project task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa159.jpg"></p>

<div align="center">

**`Fig.163` Work assignment of project tasks**

</div>

- Submitted records, click on submitted records icon shows records of work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa160.jpg"></p>

<div align="center">

**`Fig.164`  Submitted records of work assignment**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa161.jpg"></p>

<div align="center">

**`Fig.165`  Submitted records of work assignment**

</div>

### Submitted Records

* Adminsitrator can export submitted Records
  * Export to excel
  * Export to Mail
  * Export to PDF
  * See on map
  * Re-assign


To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)

### Work flow history

For the project tasks that are associated with workflow, there are two types of work flow levels:

  1. Task level

  2. Record level 

#### Task level:

To view Work flow history for project tasks that are associated with work flow in task level, click on the work flow history icon on the project task hover.

Opens work flow history page with work flow details, status, comments. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa189.jpg"></p>

<div align="center">

**`Fig.166` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa190.jpg"></p>

<div align="center">

**`Fig.167` Workflow history**

</div>

To view work flow history of project tasks that are assigned to record level work flow, in submitted records preview, click on actions and click on work flow history icon. 

Opens work flow history page with work flow details, status, comments.

#### Record level:

- In record level each record have work flow.

- Click on work assignments of project task with record level work flow.

- Click on submitted records icon, shows submitted records of work assignments.

- Click on record status of a record, shows preview of the submitted record.

- Click on actions icon.

- On clicking on actions icon, shows options work flow history, sketching, geo tagged images.

- Click on work flow history icon.

- Shows work flow history page showing work flow details, status and comments.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa191.jpg"></p>

<div align="center">

**`Fig.168` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa192.jpg"></p>

<div align="center">

**`Fig.169` Workflow history**

</div>

### Re-assign the data

Re-assign function defines that, if any record contains invalid or incomplete data then Account administrator re-assign the data to any user (if required).

To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa193.jpg"></p>

<div align="center">

**`Fig.170` Reassign records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa194.jpg"></p>

<div align="center">

**`Fig.171`  Reassign records**

</div>

Once the record is re-assigned, then the record status  be changed to Reassigned record.
