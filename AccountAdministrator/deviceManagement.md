

1.	Once the Account administrator logs in, "Device Management" icon is visible in left side menu.

 -	All the devices belonging to all the account  visible on clicking on "Device Management" tab.

 -	Any user trying to login form any device for the first time, needs his device to be approved by the Account administrator.

 -	When the request for a new device comes, its status is "Pending". Account administrator needs to check for the credibility and take necessary action.

<p align="center"> <img width="1000" height="450" src="media/SU/SU219.jpg"> </p>

<div align="center"> 

**`Fig.32` Device management page**

</div>

 -	We get the list of "Pending" devices from the device icon in the header.

 -	Account administrator can Approve/Suspend/Revoke/Unauthorize a particular device request based on the credibility.

2.	Account administrator can apply filter bases on status

The following are the status:

 1)	All

 2)	Approved

 3)	Pending

 4)	Rejected

 5)	Suspended

 6)	Unauthorized

<p align="center"> <img width="1000" height="450" src="media/SU/SU220.jpg"> </p>

<div align="center"> 

**`Fig.33` Device management page**

</div>

-	If Account administrator selects "All" option from filter

<p align="center"> <img width="1000" height="450" src="media/SU/SU221.jpg"> </p>

<div align="center"> 

**`Fig.34` Filter device details**

</div>

-	Account administrator can see all the devices with its status as Pending/Rejected/Approved/Suspended/Unauthorized.

-	If Account administrator selects "Approved", then Account administrator can see all the approved devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU222.jpg"> </p>

<div align="center"> 

**`Fig.35` Filter device details**

</div>

-	If Account administrator selects "Pending", then Account administrator can see all the pending devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU223.jpg"> </p>

<div align="center"> 

**`Fig.36` Filter device details**

</div>

-	If Account administrator selects "Rejected", then Account administrator can see all the rejected devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU224.jpg"> </p>

<div align="center"> 

**`Fig.37` Filter device details**

</div>

-	If Account administrator selects "suspended", then Account administrator can see all the suspended devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU225.jpg"> </p>

<div align="center"> 

**`Fig.38` Filter device details**

</div>

-	If Account administrator selects "unauthorized", then Account administrator can see all the unauthorized devices.

<p align="center"> <img width="1000" height="450" src="media/SU/SU226.jpg"> </p>

<div align="center"> 

**`Fig.39` Filter device details**

</div>

3.	Account administrator can approve the device

-	If Account administrator, click on status of device, it shows the list as approve, Reject, Unauthorize.

<p align="center"> <img width="1000" height="450" src="media/SU/SU227.jpg"> </p>

<div align="center"> 

**`Fig.40` Device management page**

</div>

-	If Account administrator clicks on Approve, it shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU228.jpg"> </p>

<div align="center"> 

**`Fig.41` Approve Device**

</div>

-	If Account administrator clicks on "Yes" then the device  get approved and toasted message  shown as "Device is approved" and send auto-generate approval mail to field user.

<p align="center"> <img width="1000" height="450" src="media/SU/SU229.jpg"> </p>

<div align="center"> 

**`Fig.42` Approve device**

</div>

-	The status  changed from Pending to Approved.

-	If Account administrator clicks on "No" then the device  not get approved and shows a toasted message as "Device approval action is cancelled"

<p align="center"> <img width="1000" height="450" src="media/SU/SU230.jpg"> </p>

<div align="center"> 

**`Fig.43` Cancel approve device**

</div>

5.	Account administrator can reject the device details

 -	On click status dropdown, Account administrator can see Approve, Reject, unauthorized

 -	On selection of Reject, Account administrator can reject the device details 

<p align="center"> <img width="1000" height="450" src="media/SU/SU231.jpg"> </p>

<div align="center"> 

**`Fig.44` Reject device**

</div>

-	On click reject, Account administrator can see the rejection alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU232.jpg"> </p>

<div align="center"> 

**`Fig.45` Reject device**

</div>

-	Shows the alert to confirm the device details

-	If Account administrator clicks on "Yes"

<p align="center"> <img width="1000" height="450" src="media/SU/SU233.jpg"> </p>

<div align="center"> 

**`Fig.46` Reject device**

</div>

-	Device Rejected Successfully

-	If device status is rejected then Account administrator can delete the device details.

-	Click on delete icon, then shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU234.jpg"> </p>

<div align="center"> 

**`Fig.47` Delete device details**

</div>

-	If click on "Yes", then device details  be deleted, and toasted message shown as "Device deleted successfully".

<p align="center"> <img width="1000" height="450" src="media/SU/SU235.jpg"> </p>

<div align="center"> 

**`Fig.48` Delete device details**

</div>

-	If click on "No", then device details  be in rejected status and toasted message  be shown as "Device deletion is cancelled" and all the device details page  be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU236.jpg"> </p>

<div align="center"> 

**`Fig.49` Cancel delete device details**

</div>

<p align="center"> <img width="1000" height="450" src="media/SU/SU237.jpg"> </p>

<div align="center"> 

**`Fig.50` Device management page**

</div>

-	Device Reject process gets cancelled.

5.	Account administrator can Unauthorized the device

 -	Account administrator need to click on status dropdown

 -	Account administrator can unauthorize the device details by selecting unauthorize option


<p align="center"> <img width="1000" height="450" src="media/SU/SU238.jpg"> </p>

<div align="center"> 

**`Fig.51` Unauthorize Device**

</div>

-	On click Unauthorized, Device Unauthorize process shows the confirmation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU239.jpg"> </p>

<div align="center"> 

**`Fig.52` Unauthorize Device**

</div>

-	If Account administrator clicks on "No", Device Unauthorize process  cancelled

<p align="center"> <img width="1000" height="450" src="media/SU/SU240.jpg"> </p>

<div align="center"> 

**`Fig.53` Cancel Unauthorize Device**

</div>

-	If Account administrator clicks on "Yes", Device  be unauthorized, and device can be deleted by clicking on delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU241.jpg"> </p>

<div align="center"> 

**`Fig.54` Device management page**

</div>

-	Account administrator can delete the unauthorized devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/SU242.jpg"> </p>

<div align="center"> 

**`Fig.55` Delete device details**

</div>

-	On click delete icon, Deletion confirmation alert displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/SU243.jpg"> </p>

<div align="center"> 

**`Fig.56` Delete device details**

</div>

-	If click on "Yes", then device  deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/SU244.jpg"> </p>

<div align="center"> 

**`Fig.57` Delete device details**

</div>

-	If click on "No", deletion process be cancelled.

6.	Account administrator can suspend the device details.

If the device status is approved and Administrator wants to Suspend then Account administrator clicks on Approve and it shows the suspend option as follows

<p align="center"> <img width="1000" height="450" src="media/SU/SU245.jpg"> </p>

<div align="center"> 

**`Fig.58` Device management page**

</div>

If Account administrator clicks on Suspend.

<p align="center"> <img width="1000" height="450" src="media/SU/SU246.jpg"> </p>

<div align="center"> 

**`Fig.59` Suspend Device**

</div>

If Account administrator clicks on "No", process  cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU247.jpg"> </p>

<div align="center"> 

**`Fig.60` Cancel suspend Device**

</div>

If Account administrator clicks on "Yes", device  be suspended

<p align="center"> <img width="1000" height="450" src="media/SU/SU248.jpg"> </p>

<div align="center"> 

**`Fig.61` Suspend Device**

</div>

Changes the device status  be changed from Approved to suspended.

<p align="center"> <img width="1000" height="450" src="media/SU/SU249.jpg"> </p>

<div align="center"> 

**`Fig.62` Device management page**

</div>

Account administrator can delete the suspended devices by clicking in delete icon.

<p align="center"> <img width="1000" height="450" src="media/SU/suspended devices.jpg"> </p>

<div align="center"> 

**`Fig.63` Delete device details**

</div>

-	On click delete icon, Deletion confirmation alert  be displayed.

<p align="center"> <img width="1000" height="450" src="media/SU/delete device.jpg"> </p>

<div align="center"> 

**`Fig.64` Delete device details**

</div>

-	If click on "Yes", then device  be deleted successfully.

<p align="center"> <img width="1000" height="450" src="media/SU/deleted toast.jpg"> </p>

<div align="center"> 

**`Fig.65` Delete device details**

</div>

-	If click on "No", deletion process  be cancelled.

7.	Account administrator can update the status from suspend to revoke back to pending.

<p align="center"> <img width="1000" height="450" src="media/SU/SU250.jpg"> </p>

<div align="center"> 

**`Fig.66` Revoke device status**

</div>

Click on revoke, shows the conformation alert.

<p align="center"> <img width="1000" height="450" src="media/SU/SU251.jpg"> </p>

<div align="center"> 

**`Fig.67` Revoke device status**

</div>

If click on "No", revoke process is cancelled.

<p align="center"> <img width="1000" height="450" src="media/SU/SU252.jpg"> </p>

<div align="center"> 

**`Fig.68` Cancel revoke device**

</div>

If click on "Yes", device details is revoked successfully, and toasted message  shown as "your device is pending".

<p align="center"> <img width="1000" height="450" src="media/SU/SU253.jpg"> </p>

<div align="center"> 

**`Fig.69` Device management page**

</div>

when a suspended device is revoke, status changes to pending. 

## Device Management Activity

1. Click on activity tab in device management page.

2. Application will open activity page of device management with from date, to date and filter icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/device activity.jpg"></p>

<div align="center">

</div>

3. Choose From date and To date and click on filter icon.

4. Device Activity details in between the selected dates gets displayed.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/device activity filter.jpg"></p>

<div align="center">

</div>
