
Workflow Management is the feature provided by FieldOn which we can co-ordinate and sequence the tasks.

### Workflow creation

- On clicking the "Workflow Management" tab, we can see the list of workflows created previously.

- Click on the "Create" button which would open a page to enter details for workflow creation.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa254.jpg"></p>

<div align="center">

**`Fig.186` Create workflow**

</div>

- Fill the required details, you can add the levels, and then create the workflow.

- After successful creation you can see the created workflow in the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa255.jpg"></p>

<div align="center">

**`Fig.187` Create workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa256.jpg"></p>

<div align="center">

**`Fig.188`  Workflow creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa257.jpg"></p>

<div align="center">

</div>

### Workflow Edit and level deletion

- You can edit a workflow if the workflow is not yet assigned to any task or form.

- Click on "Edit" button and you can change the "From" and "To" values and click on save. Updates the workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa258.jpg"></p>

<div align="center">

**`Fig.189` Edit workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa259.jpg"></p>

<div align="center">

</div>

- If the workflow is not yet assigned to any task or form, the level of the workflow can also be deleted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa260.jpg"></p>

<div align="center">

</div>

### Workflow deletion

- If the workflow is not assigned to any task or form, it can be deleted.

- Click on the "Delete" button and, on confirmation message click "Yes".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa261.jpg"></p>

<div align="center">

**`Fig.190` Delete workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa262.jpg"></p>

<div align="center">

**`Fig.191` Delete workflow**

</div>

- The workflow can be deleted. The application shows message as "workflow deleted successfully.