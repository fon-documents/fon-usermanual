

Click on [*work flow approval cycle*](/WorkflowCycle/workflowApprovalCycle.md) for more information.
 We have two types of approval cycles for workflow in FieldOn.

 - Task level workflow

 - Record level workflow

### Task Level Workflow

The "Task Level Workflow" is the one assigned to the "Tasks", the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

Here we take an example of Task Level Workflow on a task. (Note: We have used same workflow as used in task level workflow). 

 - After field user has submitted the assigned records, the workflow gets triggered and the task goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on "Submitted Record" icon.
 
 - Basically, work flow starts from group admin to moderator. After group admin has approved the task after checking the submitted records of that task's work assignment, moderator can approve or reject that task in approvals tab. 

 - Now when "Moderator" logs in, can find the task in approvals tab, showing as waiting for approval. 
 
 - After moderator approves the records of task's work assignment, next approver in the work flow will get access to accept or reject records.

 - If the first person of the work flow rejects the task, the task was reassigned to the field user.

- While creating a task, we assign workflow to it as follows,

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa263.jpg"></p>

<div align="center">

</div>

- Select the type of workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa264.jpg"></p>

<div align="center">

**`Fig.192` Workflow selection**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa265.jpg"></p>

<div align="center">

**`Fig.193`  Workflow level selection**

</div>

- After you have created the task, and assigned the workflow to it, we can now create assignments for this task.

- For creation of assignment please check the topic "Work Assignments".

- The data is submitted on these assignments by the field user.

- After the data is submitted, the Account Administrator, needs to change the status of the task, to "Workflow Cycle Started", then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.

- Example: The following is the workflow used for depicting the task level workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa266.jpg"></p>

<div align="center">

**`Fig.194`  Edit status**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa267.jpg"></p>

<div align="center">

**`Fig.195`  No.of days waiting for approval**

</div>

## Workflow Cycle Started:

- As in the above workflow, "Chanda GA" is the first person, he find this task in the tasks pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa268.jpg"></p>

<div align="center">

**`Fig.196`  Workflow approval**

</div>

- On clicking on the above highlighted icon, you can find the recent task "Ballarpur Area" pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa269.jpg"></p>

<div align="center">

**`Fig.197`  Approval process**

</div>

- Now if the "Chanda GA" rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.

- When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of "Chanda GA"

- Now the "Chanda GA" approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to "Moderator Chandrapur" (who is a moderator).

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa270.jpg"></p>

<div align="center">

**`Fig.198` Approval process**

</div>

- Now the task comes to the next level, which is at "Moderator Chandrapur".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa271.jpg"></p>

<div align="center">

**`Fig.199` Approval process**

</div>

- "Moderator Chandrapur" can accept or reject the task, if moderator accepts, it goes to the next level, else back to the earlier level.

- Now on accepting, it goes to "Chanda GA" (Note: Please see the from and to for all levels in the workflow)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa272.jpg"></p>

<div align="center">

</div>

- Again, the "Chanda GA"  be responsible to approve or reject the task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa273.jpg"></p>

<div align="center">

</div>

- On approving the workflow cycle completes.

## Record level Workflow:

The "Record Level Workflow" is the one assigned on the "Tasks", the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

Here we take an example of Record Level Workflow on a task.

 - Whenever any record is submitted for this task's work assignment, the workflow gets triggered and the record goes to the person at the first level of the workflow. Can find the records submitted for work assignment of the task by clicking on "Submitted Record" icon.

 - Basically, work flow starts from group admin to moderator. After group admin has approved the records of a task's work assignment which is assigned to record level work flow, moderator can be approve or reject records of that task's work assignment in approvals tab.

 - Now when "Moderator" logs in, can find the record in RED color in forms records, waiting for approval. 

 - After moderator approves the records of task's work assignment, next approver in the work flow will get access to accept or reject records.

 - If the record is rejected by first approval in the work flow, record was reassigned to the field user.

- We assign workflow to a form by editing the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa274.jpg"></p>

<div align="center">

</div>

- Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on "Submitted Record" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa275.jpg"></p>

<div align="center">

</div>

- Now login as "Chanda GA" and go to the forms records,  find the record status in RED color. The RED color indicates that the record is pending for the logged in user's approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa276.jpg"></p>

<div align="center">

</div>

- On clicking on the status, the record details open in the side panel. There on clicking on the ellipse,  find accept or reject icons.

##   Record level Approval flow

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa277.jpg"></p>

<div align="center">

**`Fig.200` Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa278.jpg"></p>

<div align="center">

**`Fig.201`  Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa279.jpg"></p>

<div align="center">

</div>

- On rejecting the record, it goes back to the field user for resubmission.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa280.jpg"></p>

<div align="center">

</div>

- When field user submits the data, again the record comes to the "Chanda GA" for approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa281.jpg"></p>

<div align="center">

</div>

- Now when "Moderator Chandrapur" logs in, he can find the record in RED color in forms records, waiting for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa282.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa283.jpg"></p>

<div align="center">

</div>

- The next level person, "Chanda GA", now has to accept or reject the record.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa284.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa285.jpg"></p>

<div align="center">

</div>