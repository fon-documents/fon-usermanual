

Templates are created by super administrator, so that account administrators can import the template as form and then modify and use according to their requirement for their projects/work assignments. 

Account administrator can search, filter, group templates by category, preview, import. 

- When clicked on Templates tab, we can see the list of Templates available.


### Preview Template

1..	Click on preview icon, opens preview template page

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa73.jpg"></p>

<div align="center">

**`Fig.70` Preview of template**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa74.jpg"></p>

<div align="center">

**`Fig.71` Preview of template**

</div> 

### Import Template as Form

1.	To import a template as form, go to the template and hover on the template we can see Import button. 

2.	Click on it to import the Template.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa75.jpg"></p>

<div align="center">

**`Fig.72` Click on import**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa76.jpg"></p>

<div align="center">

**`Fig.73` Import template as form**

</div> 

3.	We can see the existing data of the template where we need to change name, select form type (public/private), When we select form type as private, we need to select users),select the category in that Account and click on Go button. Shows imported successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa77.jpg"></p>

<div align="center">

</div> 

4.	Template  be imported into Forms tabs. Shows forms tabs after imported successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa78.jpg"></p>

<div align="center">

</div> 

### Search and filter templates

1.	Account administrator can search templates, shows the available templates with searched name.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa66.jpg"></p>

<div align="center">

**`Fig.74` Search Template**

</div> 

2.	Filter templates, click on filter icon, select the type (All/public/private) shows the templates based on the selection, by default shows all templates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/f1.jpg"></p>

<div align="center">

**`Fig.75` Filter Template**

</div> 

3. Select filter type as public, shows only public templates

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/f2.jpg"></p>

<div align="center">

**`Fig.76` Filter Templates by public type**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/f3.jpg"></p>

<div align="center">

**`Fig. 77` Filter Templates by public type**

</div>

4. Select filter type as private, shows only private templates

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/f4.jpg"></p>

<div align="center">

**`Fig.78` Filter Templates by private type**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/f5.jpg"></p>

<div align="center">

**`Fig.79` Filter Templates by private type**

</div>

5.	Group by category, click on group by category icon, shows categories and templates under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa70.jpg"></p>

<div align="center">

**`Fig` Group by category of Templates**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa71.jpg"></p>

<div align="center">

**`Fig.80` View Templates under each category**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa72.jpg"></p>

<div align="center">

</div> 





