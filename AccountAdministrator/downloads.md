

- The "Downloads" tab provides the feasibility to download the record reports, that is both excel and pdf reports.

- It has the other details like File name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.

- The files under "Downloads" tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "10000 "height ="450" src = "media/SU/SU254.jpg"></p>

<div align="center">

**`Fig.202` Downloads page**

</div>