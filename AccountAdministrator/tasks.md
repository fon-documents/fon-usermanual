﻿
Tasks are defined as the Account administrator can  assign the work to group of users along with some predefined data (if required).

 - For assigning the task, first go to ‘Tasks tab’ and click on ellipse, click on create

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa195.jpg"></p>

<div align="center">

**`Fig.172` Create task**

</div>

- After clicking on ‘Create’ button, now enter the task details, like ‘Name, Description, Category, Group administrator, Workflow, Workflow Level, Start date, End date’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa196.jpg"></p>

<div align="center">

**`Fig.173` Create task form**

</div>

- Once the details are entered now click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa197.jpg"></p>

<div align="center">

**`Fig.174` Create tasks**

</div>

- Once the task is created application shows a pop-up message saying, "Task created successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa198.jpg"></p>

<div align="center">

</div>

### Work assignment creation

- After creating the task click on ‘Work assignment option’ to assign the users.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa199.jpg"></p>

<div align="center">

**`Fig.175` Work assignment creation**

</div>

- Now click on ‘+ button’ which is to create a new work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa200.jpg"></p>

<div align="center">

**`Fig.176` Work assignment creation**

</div>

- Once after clicking on new work assignment creation, the application ask whether to upload pre-populated data ‘Yes or No’.

### Work assignment creation- without pre-populated data

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa201.jpg"></p>

<div align="center">

</div>

- Now click on No and continue.

- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.

- Once after entering all the details now click on create button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa202.jpg"></p>

<div align="center">

**`Fig.177` Work assignment without pre-papulated data**

</div>

- Once after clicking on create button the application displays a toast message as "Work assignment created successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa203.jpg"></p>

<div align="center">

</div>

### Work assignment creation – with pre-populated data

Account administrator can create work assignments by assigning few pre-populated records to field users. Field users assigned to that work assignment can add few more data for those records and submit them.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa201.jpg"></p>

<div align="center">

**`Fig.178` Work assignment creation with pre-papulated data**

</div>

- Click on yes, and continue.

- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.

- Select the form and upload the selected form Excel file with pre-populated data.

- Once after entering all the details now click on next button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa205.jpg"></p>

<div align="center">

**`Fig.179` Work assignment creation with pre-papulated data**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa206.jpg"></p>

<div align="center">

**`Fig.180` Assign pre-papulated data to users**

</div>

- Once after clicking on create button the application display a message saying, Work assignment created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa207.jpg"></p>

<div align="center">

</div>

### Edit Task

- Account administrator can edit/ update the created task (if required) by clicking on ‘Edit’ option. Account administrator can update few details like "Description, Category, Group Admin, End date". 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa208.jpg"></p>

<div align="center">

**`Fig.181` Edit task**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa209.jpg"></p>

<div align="center">

</div>

!>Account administrator cannot update the assigned Users, assigned Forms, assigned data.

- After editing the details for task click on ‘Update button’ to save the changes. The application  display a message saying, ‘Task updated successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa210.jpg"></p>

<div align="center">

</div>

### Delete Task

- Account administrator can delete the task only if the start date of the task is not current date, or if no users are assigned to the Task.

- If the task is in-progress, then Account administrator unable to delete the task & application display a message saying, ‘Task is in progress cannot be deleted’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa211.jpg"></p>

<div align="center">

</div>

### Submitted Records

1.	Submitted records define to the Account administrator, that all the user submitted records displayed. Account administrator can export the data to PDF, Excel.

2.	To get the submitted data click on the required task and then ‘Work assignment’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa212.jpg"></p>

<div align="center">

**`Fig.182` List of tasks**

</div>

3.	Now within the work assignment click on ‘Submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa213.jpg"></p>

<div align="center">

**`Fig.183`  List of work assignments**

</div>

4.	All the submitted data by the users for the work assignment  be displayed as a list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa214.jpg"></p>

<div align="center">

**`Fig.184` Submitted records of work assignments**

</div>

* Adminsitrator can export submitted Records
  * Export to excel
  * Export to Mail
  * Export to PDF
  * See on map
  * Re-assign


To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)

### Pending Records

- Click on pending records button which is on the work assignments hover.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa234.jpg"></p>

<div align="center">

**`Fig.185` Pending records**

</div>

- After clicking on pending records, shows pending records with the pre-populated data. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa235.jpg"></p>

<div align="center">

</div>

Adminsitrator can export pending Records 
   * Export to excel
   * Export to Mail
   * Export to PDF
   * Add Records
   
To know about more information click on [**Pending Records**](/PendingRecords/Pendingrecords.md)