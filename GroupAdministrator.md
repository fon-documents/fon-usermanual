
# Group Admnistrator
<!-- {docsify-ignore} -->
Group administrator  is created by the Super Administrator or Account administrator. Group administrator can be mapped to "n" number of Accounts and one Account can have multiple group administrators. So, group administrator can view all the projects and tasks information mapped to the respective group administrator accounts.
Group administrators are involved in the workflow to review the data submitted by the field users.
											
<p align="center"> <img width="1000" height="500" src="media/GAA.jpg"> </p>

<div align="center"> 

**`Fig` Group administrator activities**

</div>

<p>Enter valid Group administrator username and password and click on "Sign in".</p>

Login as [Group administrator ](https://demo.fieldon.com/#/login)

<p align="center"> <img width="1000" height="450" src="media/GA/ga1.jpg"> </p>

<div align="center"> 

**`Fig.1` Group Administrator login page**

</div>

- After login we can see the Group administrator page, list of below options in the menu bar.

 - `Users`	

 - `Forms`

 - `Projects`
 
 - `Task`

 - `Downloads`

<hr>








































































































































