Group Administrator can view forms information, submitted date and can also review data for forms which have workflow assoicated with group administrator.

1.	When Group Administrator clicks on the Form's tab, the group administrator can see the Forms available.

2.	Account Administrator can create Forms. Group Administrator cannot create/edit the forms.

<p align="center"> <img width="1000" height="450" src="media/GA/ga6.jpg"> </p>

<div align="center"> 

**`Fig.6` List of Forms**

</div>

### Form Preview

1.	To Preview the Form, go to the form and hover on the form. Group admin can see the preview button. 

2.	Click on "preview button" a popup  be open with all the widgets in the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga7.jpg"> </p>

<div align="center"> 

**`Fig.7` Preview Forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga8.jpg"> </p>
<div align="center"> 

**`Fig.8` Preview Forms**

</div>

### Form Export to Excel

1.	Group admin can Export the Form to Excel to get all the widgets of the form available in the form of excel. By clicking the excel icon. 

2.	Group admin can use this excel and upload in Work Assignments to the user. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga9.jpg"> </p>

<div align="center"> 

**`Fig.9` Export to Excel**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga10.jpg"> </p>

<div align="center"> 

**`Fig.10` Excel File**

</div>


### Submitted forms

1.	Click on the "submitted icon" to view the user submitted forms records.

2.	Once all the records are displayed, Administrator can export the data to PDF, Excel, Export to Email, Re-assign data, Go to Map view. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga11.jpg"> </p>

<div align="center"> 

**`Fig.11` View submitted forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga12.jpg"> </p>

<div align="center"> 

**`Fig.12` View submitted forms**

</div>

* Group adminsitrator can export submitted Records
  * Export to excel
  * Export to Mail
  * Export to PDF
  * See on map
 
To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)
 
### Form Info

1. Group administrator can see the information about the form by clicking on "info" option

<p align="center"> <img width="1000" height="450" src="media/GA/ga27.jpg"> </p>

<div align="center"> 

**`Fig.13`Form Info**

</div>

2. It show the pop-up menu with details ,form name, created by, creation date, No of assignments, No of cases collected, last form modification, last downloaded date, last downloaded by, No of users downloaded.

<p align="center"> <img width="1000" height="450" src="media/GA/ga28.jpg"> </p>

<div align="center"> 

**`Fig.14`Form Information**

</div>

### Form Filter

1.	It shows the forms with different types, like public and private forms 

<p align="center"> <img width="1000" height="450" src="media/GA/ga29.jpg"> </p>

<div align="center"> 

**`Fig.15`Form Filter**

</div>

2.	By selecting public in list it filter out public type forms in the list, and it shows with blue colour indication.

<p align="center"> <img width="1000" height="450" src="media/GA/ga30.jpg"> </p>

<div align="center"> 

**`Fig.16`Form with color indication**

</div>
