
The Dashboards provide the statistical information present under the login. 

You can find the dashboard icon present in the right middle of the screen edge throughout all the pages.

Under group adnministartor three type of entities:

-	Projects

-	Tasks

-	Users

1. Under the "Projects" entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga139.jpg"> </p>

<div align="center"> 

**`Fig.62`Dashboard**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga140.jpg"> </p>

<div align="center"> 

**`Fig.63`Project entity dashboard**

</div>

-	Here Completed project block displays the No of project completed.

-	Pending Project block displays the No of projects in pending state.

-	Projects functionality block displays the total No of the projects under group administrator.

-	Work assignments block displays the total No of work assignments under group administrator.

-	In completed project list we can the projects that are completed in the form of the list.

2.	Under the "Tasks" entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga141.jpg"> </p>

<div align="center"> 

**`Fig.64`Task entity**

</div>

-	Here Completed project block displays the No of project completed.

-	The pending task block displays the No of the task are in pending state.

-	Task block displays the total No of task created.

3. Under the "Users" entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga142.jpg"> </p>

<div align="center"> 

**`Fig.65` User's entity**

</div>

-	Active users block will display the total No of active users.

-	The users block will displays the total No of users present.

-	In active users block which is in right side, by clicking on ellipse button. The administrator  see the active users with Username, Logged Time and Device details in the list.

