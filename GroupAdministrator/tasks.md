
The group administrator can be able to see the task, which the account administrator creates. By click on the Task tab, it will show the associated task in the list. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga108.jpg"> </p>

<div align="center"> 

**`Fig.34`Task tab screen**

</div>

### Preview Task

1.	Administrator can preview task (if required) by clicking on "preview" option. The administrator can see all the  data in fields. Those fields are  read and only fields.

<p align="center"> <img width="1000" height="450" src="media/GA/ga109.jpg"> </p>

<div align="center"> 

**`Fig.35`Preview task**

</div>

### Submitted Records

1.	Submitted records define to the Administrator, that all the user submitted records displayed. A group administrator can export the data to PDF, Excel.

2.	To get the submitted data click on the required task and then 'Work assignment'.

<p align="center"> <img width="1000" height="450" src="media/GA/ga110.jpg"> </p>

<div align="center"> 

**`Fig.36`Click on work assignment**

</div>

* Adminsitrator can export submitted Records
  * Export to excel
  * Export to Mail
  * Export to PDF
  * See on map
  * Re-assign

To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)