
We have two types of approval cycles for workflow in FieldOn:

-	Task level workflow

-	Record level workflow

### Task Level Workflow

The "Task Level Workflow" is the one assigned to the "Tasks", the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

Here we take an example of Task Level Workflow on a task. (Note: We have used same workflow as used in task level workflow). 

 - After field user has submitted the assigned records, the workflow gets triggered and the task goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on "Submitted Record" icon.
 
 - Basically, work flow starts from group admin to moderator. After group admin has approved the task after checking the submitted records of that task's work assignment, moderator can approve or reject that task in approvals tab. 

 - Now when "Moderator" logs in, can find the task in approvals tab, showing as waiting for approval. 
 
 - After moderator approves the records of task's work assignment, next approver in the work flow will get access to accept or reject records.

 - If the first person of the work flow rejects the task, the task was reassigned to the field user.

<p align="center"> <img width="1000" height="450" src="media/GA/ga83.jpg"> </p>

<div align="center"> 

**`Fig.37`Selection of workflow**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga84.jpg"> </p>

<div align="center"> 

**`Fig.38`Selection of workflow level**

</div>

1.	While creating a task, we assign workflow to it as follows ,

2.	Select the type of workflow.

3.  After you have created the task, and assigned the workflow to it, we can now create assignments for this task.

4.	For creation of assignment please check the topic "Work Assignments".

5.	The data is submitted on these assignments by the field user.

6.	After the data is submitted, the Account Administrator, needs to change the status of the task, to "Workflow Cycle Started", then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.

7.	Example: The following is the workflow used for depicting the task level workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga85.jpg"> </p>

<div align="center"> 

**`Fig.39`Workflow cycle**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga86.jpg"> </p>

<div align="center"> 

**`Fig.40`Status of project**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga87.jpg"> </p>

<div align="center"> 

**`Fig.41`Displays  the  status of project**

</div>

### Workflow Cycle Started

-	As in the above workflow, "Chanda GA" is the first person, find this task in the tasks pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga88.jpg"> </p>

<div align="center"> 

**`Fig.42`Workflow Notificationt**

</div>

-	On clicking on the above highlighted icon, you can find the recent task "Ballarpur Area" pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga89.jpg"> </p>

<div align="center"> 

**`Fig.43`Displays  the approvals projects**

</div>

-	Now if the "Chanda GA" rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.

-	When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of "Chanda GA"

-	Now the "Chanda GA" approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to "Moderator Chandrapur" (who is a moderator).


<p align="center"> <img width="1000" height="450" src="media/GA/ga90.jpg"> </p>

<div align="center"> 

**`Fig.44`Shows the next approver person in cycle**

</div>

-	Now the task comes to the next level, which is at "Moderator Chandrapur".

<p align="center"> <img width="1000" height="450" src="media/GA/ga91.jpg"> </p>

<div align="center"> 

**`Fig.45`Workflow cycle in moderator**

</div>

-	"Moderator Chandrapur" can accept or reject the task, if he accepts, it goes to the next level, else back to the earlier level.

-	Now on accepting, it goes to "Chanda GA"(Note: Please see the from and to for all levels in the workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga92.jpg"> </p>

<div align="center"> 

**`Fig.46`Approval in moderator**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga93.jpg"> </p>

<div align="center"> 

**`Fig.47`Approval in Group administrator**

</div>

-	Again, the "Chanda GA"  be responsible to approve or reject the task.

-	On approving the workflow cycle completes.

<p align="center"> <img width="1000" height="450" src="media/GA/ga94.jpg"> </p>

<div align="center"> 

**`Fig.48`Shows next Approval**

</div>

### Record Level Workflow

The "Record Level Workflow" is the one assigned on the "Tasks", the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

Here we take an example of Record Level Workflow on a task.

 - Whenever any record is submitted for this task's work assignment, the workflow gets triggered and the record goes to the person at the first level of the workflow. Can find the records submitted for work assignment of the task by clicking on "Submitted Record" icon.

 - Basically, work flow starts from group admin to moderator. After group admin has approved the records of a task's work assignment which is assigned to record level work flow, moderator can be approve or reject records of that task's work assignment in approvals tab.

 - Now when "Moderator" logs in, can find the record in RED color in forms records, waiting for approval. 

 - After moderator approves the records of task's work assignment, next approver in the work flow will get access to accept or reject records.

 - If the record is rejected by first approval in the work flow, record was reassigned to the field user.


1.	We assign workflow to a form by editing the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga95.jpg"> </p>

<div align="center"> 

**`Fig.49`Assign work flow**

</div>

2.	Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on "Submitted Record" icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga96.jpg"> </p>

<div align="center"> 

**`Fig.50`Select the record to be approved**

</div>

3.	Now login as "Chanda GA" and go to the forms records,  find the record status in RED color. The RED color indicates that the record is pending for the logged in user's approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga97.jpg"> </p>

<div align="center"> 

**`Fig.51`Here red in colour shows record is pending to approve**

</div>

4.	On clicking on the status, the record details open in the side panel. There on clicking on the ellipse,  find accept or reject icons.

5.	On rejecting the record, it goes back to the field user for resubmission.

<p align="center"> <img width="1000" height="450" src="media/GA/ga98.jpg"> </p>

<div align="center"> 

**`Fig.52`Click on update to approve/reject  record**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga99.jpg"> </p>

6.	When field user submits the data, again the record comes to the "Chanda GA" for approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga100.jpg"> </p>

<div align="center"> 

**`Fig.53`When user submits data  again records  come to  groupadmin**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga101.jpg"> </p>

<div align="center"> 

**`Fig.54`Message displayed after successful approval**

</div>

7.	Now when "Moderator Chandrapur" logs in, he can find the record in RED color in forms records, waiting for his approval

<p align="center"> <img width="1000" height="450" src="media/GA/ga102.jpg"> </p>

<div align="center"> 

**`Fig.55`Record in pending for review in moderator as workflow**

</div>

8.	The next level person, "Chanda GA", now has to accept or reject the record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga103.jpg"> </p>

<div align="center"> 

**`Fig.56`Here the status of the record change to approved after moderator approval**

</div>


## Workflow History

### Task Level Workflow History

For any task having assigned a workflow, we can see the workflow history, that is, the series of approval or rejection done throughout the workflow cycle.

We need to click on the "Workflow history" icon to fetch these details.

<p align="center"> <img width="1000" height="450" src="media/GA/ga104.jpg"> </p>
<div align="center"> 

**`Fig.57`Hover on the task to view  task workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga105.jpg"> </p>

<div align="center"> 

**`Fig.58`Displays the task workflow history**

</div>

### Record Level Workflow History

We can see the workflow history for a record by opening the record and clicking on the "ellipse icon", we get icon to view workflow history.

<p align="center"> <img width="1000" height="450" src="media/GA/ga106.jpg"> </p>

<div align="center"> 

**`Fig.59`Record level workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga107.jpg"> </p>

<div align="center"> 

**`Fig.60`Displays record level workflow history**

</div>
