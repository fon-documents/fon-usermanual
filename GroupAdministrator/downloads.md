The "Downloads" tab provides the feasibility to download the record reports, that is both Excel and PDF reports.

It has the other details like place of download, time of download etc.

The files under "Downloads" tab gets deleted automatically after 24 hours of download.

<p align="center"> <img width="1000" height="450" src="media/GA/ga138.jpg"> </p>

<div align="center"> 

**`Fig.61`Shows exported records in PDF and Excel file formats**

</div>
