1. When clicked on the projects tab, the group admin can see the associated projects in the list.  

2. Account Administrators can only create the project. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga32.jpg"> </p>

<div align="center"> 

**`Fig.17`Shows project**

</div>

### Project view

1. To preview 'project' hover on the project,the group admin can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga33.jpg"> </p>

<div align="center"> 

**`Fig.18`Project preview**

</div>

2. Click on the preview button, view the existing data only in read-only fields, and the group Administrator cannot update project information 

<p align="center"> <img width="1000" height="450" src="media/GA/ga34.jpg"> </p>

<div align="center"> 

**`Fig.19`Shows the existing data of the project**

</div>

### Creation of Project Task

The Group administrator will create the project task within the project.  

1. To create a project task, click on the project task, click on the ellipse button, click on a project task. 

2. Click on create project task

<p align="center"> <img width="1000" height="450" src="media/GA/ga35.jpg"> </p>

<div align="center"> 

**`Fig.20`Create project task**

</div>

3. After clicking on "create", the form  open with name, Description, category, workflow, set task date, end date

<p align="center"> <img width="1000" height="450" src="media/GA/ga36.jpg"> </p>

<div align="center"> 

**`Fig.21`Project task creation form**

</div>

4. Fill the details and click on "create button"

5. After the successful creation group, the admin can see a toast message "Task created Successfully". 

<p align="center"> <img width="1000" height="450" src="media/GA/ga37.jpg"> </p>

<div align="center"> 

**`Fig.22`Message displayed after successful creation**

</div>

### Edit project task

1. To edit "project task" hover on the project task, group administartor see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga38.jpg"> </p>

<div align="center"> 

**`Fig.23`Edit Project task**

</div>

2.  Click on the edit button, view the existing data in read-only fields except for end date and category fields. If any changes there, make it and click on update. Group Administrator can update project task information.
<p align="center"> <img width="1000" height="450" src="media/GA/ga39.jpg"> </p>

<div align="center"> 

**`Fig.24`Update task**

</div>

### Delete project task

1. If you want to delete project task, click on "delete icon", the project delete only when it is not assign to user/ when it is in progress/when it has records.

2. Group Administrator can only delete project task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga40.jpg"> </p>

<div align="center"> 

**`Fig.25`Delete task**

</div>

### Create work assignments

1.	To create a work assignment, the first group administrator must create a project task as it describes as creating a project task and later clicking on the work assignment option

<p align="center"> <img width="1000" height="450" src="media/GA/ga41.jpg"> </p>

<div align="center"> 

**`Fig.26`Create work assignment**

</div>

2.	After click it opens the work assignment in project task , click on '+' to create

<p align="center"> <img width="1000" height="450" src="media/GA/ga42.jpg"> </p>

<div align="center"> 

**`Fig.27`Create work assignment**

</div>

3. It  display the pop-up menu asking to "Do you want to attach  prepop  Excel sheet?" with "yes" or "No" options, if Administrator select "yes" the menu with assignment name, form, frequency, start date, end date, upload sheet field with  choose file button to attach Excel file. Fill all the details.

4.  Click on "next""

5. If Administrator select "No" then the menu with assignment name, form, frequency, start date, end date, fill all the details

6. Click on "create""

<p align="center"> <img width="1000" height="450" src="media/GA/ga43.jpg"> </p>

<div align="center"> 

**`Fig.28`Work assignment creation form**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga44.jpg"> </p>

<div align="center"> 

**`Fig.29`Click on create-to-create work assignment**

</div>

### Edit work assignment

1. To edit "work assignment' hover on the work assignment, group administartor see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga45.jpg"> </p>

<div align="center"> 

**`Fig.30`Edit work assignment**

</div>

2.Click on the edit button, view the existing data in read-only fields expect end date. If any changes there, make it and click on update., group Administrator can update work assignment information. After a successful update group, the admin will see a toast message. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga46.jpg"> </p>

<div align="center"> 

**`Fig.31`Click on Update to update work assignment**

</div>

### Delete work assignment 

1. To delete work assignment, click on "delete icon". 

<p align="center"> <img width="1000" height="450" src="media/GA/ga47.jpg"> </p>

<div align="center"> 

**`Fig.32` Delete work assignment**

</div>

2. The group administrator cannot delete it when a work assignment is started/when it assigns to the user/when the work assignment is in progress.it shows a toast message work assignment started   

<p align="center"> <img width="1000" height="450" src="media/GA/ga48.jpg"> </p>

<div align="center"> 

**`Fig.33`Pop message  appear when assignment cannot be deleted**

</div>

### Submitted Records

  
* Adminsitrator can export submitted Records 
   * Export to excel
   * Export to Mail
   * Export to PDF
   * See on map
   * Re-assign
To know about more information click on [**Submitted Records**](/SubmittedRecords/Submittedrecords.md)

### Pending Records

Adminsitrator can export pending Records 
   * Export to excel
   * Export to Mail
   * Export to PDF
   * Add Records
   
To know about more information click on [**Pending Records**](/PendingRecords/Pendingrecords.md)

