﻿
Group administrator can view the users created by the Account administrators and can search, preview the user details. 

1.	With on click of the Users, the tab group administrator can see the list of Users available. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga2.jpg"> </p>

<div align="center"> 

**`Fig.2` User list**

</div>

### Preview Users

1.	To preview "user" hover on the user, the group administrator can see the buttons.

<p align="center"> <img width="1000" height="450" src="media/GA/ga3.jpg"> </p>

<div align="center"> 

**`Fig.3` Preview User**

</div>

2.	Click on the preview button to view the data in read-only format, and the group Administrator cannot update user information. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga4.jpg"> </p>

<div align="center"> 

**`Fig.4`  Preview User data**

</div>

### User search 

1.Enter the user’s name in the search box and click on “ →", it will sort the respective user in the user list

<p align="center"> <img width="1000" height="450" src="media/GA/ga5.jpg"> </p>

<div align="center"> 

**`Fig.5` Search User**

</div>
