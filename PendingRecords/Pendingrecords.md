# Pending Records

Administrator can assign pre-populated data to users while creating work assignment by uploading an Excel of the selected form, those pending records which are not submitted by user are visible in the pending records tab.

For pending records hover on the task, by clicking on pending records options it displays pending records for respective task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga64.jpg"> </p>

<div align="center"> 

**`Fig.1`Steps for pending records**

</div>

## Export to Excel

1.	Administrator will be able to export the pending data to excel.

First, select the data and then click on "Excel option""

<p align="center"> <img width="1000" height="450" src="media/GA/ga65.jpg"> </p>

<div align="center"> 

**`Fig.2`Steps for records to export into Excel**

</div>

2.Once after clicking on Excel option, the fields displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga66.jpg"> </p>

<div align="center"> 

**`Fig.3`Message  appear**

</div>

3.The exported data in Excel be downloaded in downloads tab.

## Export to PDF
1.	Administrator will be able to export the pending data to PDF.

2. First, select the data and then click on "PDF" option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga67.jpg"> </p>

<div align="center"> 

**`Fig.4`Steps to export records into PDF**

</div>

3.	Once after clicking on "Excel option", the fields displayed now click on "green color icon".

<p align="center"> <img width="1000" height="450" src="media/GA/ga68.jpg"> </p>

<div align="center"> 

**`Fig.5`Message  appear**

</div>

4.	The exported data in PDF downloaded in downloads tab.

## Email Pending Records

1.	Administrator will be able to export the pending data Via the Email option. 

2.	 First, select the data and then click on "Email option".

<p align="center"> <img width="1000" height="450" src="media/GA/ga69.jpg"> </p>

<div align="center"> 

**`Fig.6`Steps to export records to attach mail**

</div>

3.	Once after clicking on Email option, enter the to & CC valid email Id�s. Now select the required format �PDF, Excel� and then click on "submit button".

<p align="center"> <img width="1000" height="450" src="media/GA/ga70.jpg"> </p>

<div align="center"> 

**`Fig.7`Steps to export records to attach mail**

</div>

4.	Now select the list of fields and then click on "green tick color option", the application display a message saying, �Email sent successfully�

## Upload New Pending Records

1.	Administrator can upload new records using +Button.

2.	First, click on "Add button" and now select the "choose file option".

3.	From the choose file option select the required Excel to upload the new data to the task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga71.jpg"> </p>

<div align="center"> 

**`Fig.8`Steps to add new records**

</div>

4.	Now select the "submit option" to upload the attached file and the data be inserted in the system.

<p align="center"> <img width="1000" height="450" src="media/GA/ga72.jpg"> </p>

<div align="center"> 

**`Fig.9`Steps to add new records**

</div>

> Administrator who created work assignments will have feasibility to add records in pending records page.

## Assign Users

Administrator can assign the unassigned data to the users.

1. Select the unassigned users and then click on "users".

<p align="center"> <img width="1000" height="450" src="media/GA/ga73.jpg"> </p>

<div align="center"> 

**`Fig.10`Steps to assign to users**

</div>

Now select the required user and then click on "add user" icon and the status be changed from Unassigned to username.

Administrator can assign the pending records to another user.

1.	First select the record and then click on "users".

2.	Select the required user, and then click on "user" icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga74.jpg"> </p>

<div align="center"> 

**`Fig.11`Steps to assign records to another user**

</div>

3.	The first assigned username be changed to another new user.

> Administrator who created work assignments will have feasibility to assign records in pending records page.

## Delete Pending Record

1.	Select the records individually or multiple and then click on "delete" option.

2.	The selected records be deleted from the list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga75.jpg"> </p>

<div align="center"> 

**`Fig.12`Steps to Delete record**

</div>

> Administrator who created work assignments will have feasibility to delete records in pending records page.

## Edit  Record

1.	Click on "submitted records" button in work assignment, it display the list of records which are submitted by mobile user

2.	Please tap on "original record" it opens the form with user entered data.

3.	Tap on the "toggle button" to edit the user entered details in the record

<p align="center"> <img width="1000" height="450" src="media/GA/ga76.jpg"> </p>

<div align="center"> 

**`Fig.13`Steps to edit record**

</div>

4. Administrators can make necessary changes in the data and click on the update button. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga77.jpg"> </p>

<div align="center"> 

**`Fig.14`Steps to edit record**

</div>

> Administrator who created work assignments will have feasibility to Edit records in pending records page.

 
