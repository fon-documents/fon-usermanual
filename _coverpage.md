![logo](media/FieldOnLogo.png ':size=10%')

# FieldOn User Manual<small>v.5.0.0</small>

> MagikMinds Software Services Pvt Ltd.

<!-- - Powered by [Magikminds](https://www.magikminds.com/ "Visit our Website for more info") -->
<!-- [GitHub](https://github.com/areknawo/rex) -->
[Get Started](basic.md)

<!-- ![color](#ffffff) -->
