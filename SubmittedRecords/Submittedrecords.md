## Submitted Records

1.	By clicking on submitted record options once all the records are displayed, Administrator can be  export the data to PDF, Excel, Attach as Email, Re-assign data, Go to Map view.

<p align="center"> <img width="2000" height="450" src="media/GA/ga49.jpg"> </p>

<div align="center"> 

**`Fig.1`Submitted record page**

</div>

### Preview of submitted records

1. Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.2` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.3` Preview of submitted record**

</div>

2. Click on"actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.4` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.5`  Sketching**

</div>

Click on "geo tagged images", to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.6`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.7` Geo tagged images**

</div>

Click on "attachments" to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.8`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Edit Sketching

Edit sketching used to edit field user submitted sketching in records.

By using edit sketching function administrator can sketch objects on the map. And edit sketching functionality is explained in the below steps.

1.Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa346.jpg"></p>

<div align="center">

**`Fig.9` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa347.jpg"></p>

<div align="center">

**`Fig.10` Preview of submitted record**

</div>

2. Click on "actions", shows actions like sketching, geo tagged images.

3. Click on "sketching", to see the sketching done in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa348.jpg"></p>

<div align="center">

**`Fig.11` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa349.jpg"></p>

<div align="center">

**`Fig.12`  Sketching**

</div>

4. Edit sketching 

o Click on "edit sketch" option on the left side

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.13`  Edit Sketching**

</div>

o The options displays and click on "measuring tool" to perform edit sketching operation

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.14`  Edit Sketching**

</div>

o By clicking on "measuring tool", options  be display the polyline, rectangle, polygon, edit layers, delete layers icons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa352.jpg"></p>

<div align="center">

**`Fig.15` Edit Sketching**

</div>

o If Administrator select the polyline sketching option, application allows Administrator to draw only polyline shape sketchings on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa353.jpg"></p>

<div align="center">

**`Fig.16` Draw sketching by polyline**

</div>

o Click on the "finish button" so that it end the sketching of polyline and placed on the map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa354.jpg"></p>

<div align="center">

**`Fig.17` Draw sketching by polyline**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa355.jpg"></p>

<div align="center">

**`Fig.18` Line sketching**

</div>

o Click on the "tick icon" to save the sketching, pop-up menu displays

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.19` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to save sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.20` Save sketching**

</div>

o By clicking on "yes"in pop-up menu the sketching  save successfully and message displayed"sketching update succesfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.21` Save sketching**

</div>

5. Sketching Rectangle and Polygon

o By following the above steps Administrator  sketch shapes of rectangle and polygon by using draw a rectangle and draw polygon tool options in measuring tool on map.
 
o As shown in below figures Administrator can sketch rectangle and polygon sketching diagrams on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa368.jpg"></p>

<div align="center">

**`Fig.22` Recetangle sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa369.jpg"></p>

<div align="center">

**`Fig.23` Polygon sketching**

</div>

o Administrator place a marker on the map by using "marker "option in the measuring tool.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa375.jpg"></p>

<div align="center">

**`Fig.24` Marker sketching**

</div>

6. Delete sketching

o Administrator can delete the sketching, to delete the sketching user click on the "delete" icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa359.jpg"></p>

<div align="center">

**`Fig.25` Delete sketching**

</div>

o Click on the respective sketching to get delete

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa360.jpg"></p>

<div align="center">

**`Fig.26` Delete sketching**

</div>

o Click on the "save" option, to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa361.jpg"></p>

<div align="center">

**`Fig.27` Delete sketching**

</div>

o Click on the "tick mark" option to update sketching, pop-up menu opens.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa362.jpg"></p>

<div align="center">

**`Fig.28` Save sketching**

</div>

o Click on "yes" option in the pop-up menu to update sketching

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa363.jpg"></p>

<div align="center">

**`Fig.29` Update sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.30` Update sketching**

</div>

7. Edit sketching

o Administrator click on the "edit" button on the right side of the map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.31`  Edit Sketching**

</div>

o Administrator tap on the "measuring tool option".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa351.jpg"></p>

<div align="center">

**`Fig.32`  Measuring tool**

</div>

o Administrator tap on the "edit layers option". Administrator can see sketching data are enabled in to edit mode.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa378.jpg"></p>

<div align="center">

**`Fig.33`  Edit layers**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa379.jpg"></p>

<div align="center">

**`Fig.34`  Sketching in edit mode**

</div>


o Administrator can edit by draging handles on the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa380.jpg"></p>

<div align="center">

**`Fig.35`  Drag Handles or marker**

</div>


o After changes are done administrator click on the "save button", to save the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa381.jpg"></p>

<div align="center">

**`Fig.36` Save Option**

</div>

o Click on "tick mark" lie on the right side to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa356.jpg"></p>

<div align="center">

**`Fig.37`  Update Button**

</div>

o Click on "yes" button in pop-up display menu to update the sketching.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa357.jpg"></p>

<div align="center">

**`Fig.38`  Update Button**

</div>

o Administrator  get a message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa358.jpg"></p>

<div align="center">

**`Fig.39`  Update Button**

</div>

8. Sketching full screen view

o By clicking on "full screen view" option, the map opens in full screen


<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa376.jpg"></p>

<div align="center">

**`Fig.40` Full screen view sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa377.jpg"></p>

<div align="center">

**`Fig.41` Full screen view sketching**

</div>


### Add/Edit sketching properties in the sketching

1. Administrator can add and edit sketching properties information for sketched objects on the map

2. Click on the "edit sketch" button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa350.jpg"></p>

<div align="center">

**`Fig.42`  Edit Sketching**

</div>

3. Administrator click on the sketching object, pop-up menu open with readonly form fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa370.jpg"></p>

<div align="center">

**`Fig.43`  Sketching Properties Pop-Up menu**

</div>

4. Administrator by clicking on "edit button" in sketching properties, administrator can edit/add the fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa371.jpg"></p>

<div align="center">

**`Fig.44`  Sketching Properties**

</div>

5. By clicking on "edit option", form opens on the right side with all editable fields as shown in fig.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa372.jpg"></p>

<div align="center">

**`Fig.45` Edit Sketching Properties**

</div>

6. Fill all the fields and click on "tick mark" on the top right side to update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa373.jpg"></p>

<div align="center">

**`Fig.46` Edit Sketching Properties**

</div>

7. After successfully added the properties to sketching, message displayed"Properties added succesfully."

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa374.jpg"></p>

<div align="center">

**`Fig.47` Edit Sketching Properties**

</div>

> Administrators will have access to edit sketching for the records waiting for the approval, Account administrator has feasibilty to edit sketching for any record.

### Measuring Tool

Measuring tool is used to measure sketchings on map like distance, area of line, circle, rectangle, polygon.

1. Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod83.jpg"></p>

<div align="center">

**`Fig.48` submitted records table**

</div>

2. submitted record.It shows a submitted record preview.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod84.jpg"></p>

<div align="center">

**`Fig.49` submitted recordpreview**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod85.jpg"></p>

<div align="center">

**`Fig.50` submitted record preview**

</div>

3. "ellipse button".

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod86.jpg"></p>

<div align="center">

**`Fig.51` Click on ellipse button**

</div>

4. Click on "sketching" button.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod87.jpg"></p>

<div align="center">

**`Fig.52` Click on sketching**

</div>

5. Sketching on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod88.jpg"></p>

<div align="center">

**`Fig.53` View Sketching on map**

</div>

6. Click on "draw tool icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod89.jpg"> </p>

<div align="center"> 

**`Fig.54` Click on draw tool icon**

</div>

Draw tool has 

 - Draw a polyline.

 - Draw a Ploygon.

 - Draw a rectangle.

 - Draw a marker.

 - Edit sketching.

 - Delete sketching.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod91.jpg"> </p>

<div align="center"> 

**`Fig.55` Draw tool**

</div>

7. Click on Draw a polyline icon.
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod90.jpg"> </p>

<div align="center"> 

**`Fig.56` click on draw a polyline**

</div>

8. By click on "Polyline icon" we can draw a line on map(distance between two points).

<p align="center"> <img width="1000" height="450" src="media/moderator/mod92.jpg"> </p>

<div align="center"> 

**`Fig.57` Drawn a poly line**

</div>

9. Click on "Finish" option

<p align="center"> <img width="1000" height="450" src="media/moderator/mod93.jpg"> </p>

<div align="center"> 

**`Fig.58` Click on Finish Button**

</div>

10. To measure distance between two points.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod94.jpg"> </p>

<div align="center"> 

**`Fig.59` Length of distance**

</div>

11. Click on "polygon" icon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod95.jpg"> </p>

<div align="center"> 

**`Fig.60` Click on polygon icon**

</div>

12. Draw a polygon then click on "finish button".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod96.jpg"> </p>

<div align="center"> 

**`Fig.61` Drawn polygon on map**

</div>

13. To measure area of polygon.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod97.jpg"> </p>

<div align="center"> 

**`Fig.62` Area of polygon**

</div>

14. Click on "draw a Rectangle icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod98.jpg"> </p>

<div align="center"> 

**`Fig.63` Click on draw a rectangle**

</div>
15. To draw rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod99.jpg"> </p>

<div align="center"> 

**`Fig.64` Drawn rectangle on map**

</div>

16. To measure area of rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod100.jpg"> </p>

<div align="center"> 

**`Fig.65` Area of rectangle**

</div>

17. Click on "Marker icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod101.jpg"> </p>

<div align="center"> 

**`Fig.66` click on marker icon**

</div>

18. To place a marker on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod102.jpg"> </p>

<div align="center"> 

**`Fig.67` Marker on map**

</div>

19. By click on "edit" icon. 
  
<p align="center"> <img width="1000" height="450" src="media/moderator/mod104.jpg"> </p>

<div align="center"> 

**`Fig.68` Click on Edit icon**

</div>

20. We can see drawn sketchings(polyline, polygon, Rectangle, Marker)are changed to editable mode.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod105.jpg"> </p>

<div align="center"> 

**`Fig.69` sketchings are in editable mode**

</div>

21. After edit sketchings click on "save option".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod118.jpg"> </p>

<div align="center"> 

**`Fig.70` click on save option**

</div>

<p align="center"> <img width="1000" height="450" src="media/moderator/mod119.jpg"> </p>

<div align="center"> 

**`Fig.71` saved sketchings**

</div>

22. Click on "delete icon".
 
<p align="center"> <img width="1000" height="450" src="media/moderator/mod120.jpg"> </p>

<div align="center"> 

**`Fig.72` Click on delete icon**

</div>

23. Click on "clearAll" button.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod121.jpg"> </p>

<div align="center"> 

**`Fig.73` Click on clearAll**

</div>

24. Clear all drawn sketchings on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod122.jpg"> </p>

<div align="center"> 

**`Fig.74` Clear All drawed sketchings on map**

</div>

25. Click on "Delete icon".

<p align="center"> <img width="1000" height="450" src="media/moderator/mod112.jpg"> </p>

<div align="center"> 

**`Fig.75` Click on delete icon**

</div>

26. Click on "drawn rectangle" on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod114.jpg"> </p>

<div align="center"> 

**`Fig.76` Click on drawn rectangle**

</div>

27. Delete drawn rectangle on map.

<p align="center"> <img width="1000" height="450" src="media/moderator/mod115.jpg"> </p>

<div align="center"> 

**`Fig.77` Deleted drawn rectangle**

</div>

### Search on map 

o Click on "search"icon on the map, search bar gets displayed.

o Enter address to search and click on enter.

<p align="center"> <img width="1000" height="450" src="media/SU/search map.jpg"> </p>

<div align="center"> 

**`Fig.78` Search on map**

</div>

o Searched address highlighted on map.

<p align="center"> <img width="1000" height="450" src="media/SU/highlight map.jpg"> </p>

<div align="center"> 

**`Fig.79` Search on map**

</div>

### Export to PDF 

1.	The administrator will select single or multiple records and then click on the PDF option to generate the submitted data to PDF format. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga50.jpg"> </p>

<div align="center"> 

**`Fig.80`Export to PDF**

</div>

2.	Once after clicking on "PDF" option, now all the list of fields be displayed select the green color option. The application  display a message saying, �Please check the downloaded records in downloads tab�.

<p align="center"> <img width="1000" height="450" src="media/GA/ga51.jpg"> </p>

<div align="center"> 

**`Fig.81`Select required columns**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga52.jpg"> </p>

<div align="center"> 

**`Fig.82`Toasted messages appear**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga53.jpg"> </p>

<div align="center"> 

**`Fig.83`Record data shown in PDF Format**

</div>

### Export to Excel

1.	To download the data to Excel format, select the records individually or bulk and click on the "Export Excel" button from tabular view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga54.jpg"> </p>

<div align="center"> 

**`Fig.84`Steps to generate Excel file**

</div>

2.Once after clicking on the excel option, all the fields are displayed. Select the green colour option. The application will display a message saying, �Please check the downloaded records in the downloads tab.� 

<p align="center"> <img width="1000" height="450" src="media/GA/ga55.jpg"> </p>

<div align="center"> 

**`Fig.85`Message displays**

</div>

3.	The downloaded Excel appears in this format

<p align="center"> <img width="1000" height="450" src="media/GA/ga56.jpg"> </p>

<div align="center"> 

**`Fig.86`Message displays**

</div>

### Export to Email

1.	The administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id, which is optional.  

<p align="center"> <img width="1000" height="450" src="media/GA/ga57.jpg"> </p>

<div align="center"> 

**`Fig.87`Steps to attach mail**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga58.jpg"> </p>

<div align="center"> 

**`Fig.88`Email generation form**

</div>

2.The administrator can attach records and send them to any email Id, Via., Excel, PDF. Toast messages are shown on performing email records. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga59.jpg"> </p>

<div align="center"> 

**`Fig.89`Messaged displayed after successful sent**

</div>

### Re-assign the data

1.	The re-assign function defines that if any record contains invalid or incomplete data, then the administrator will re-assign the data to any user (if required). 

2. To re-assign the record, first select the required record, and click on "Re-assign" button.

<p align="center"> <img width="3000" height="450" src="media/GA/ga60.jpg"> </p>

<div align="center"> 

**`Fig.90`Steps to re-assign record**

</div>

3.	  Enter the comments and select to which user the record must reassign and click on submit

<p align="center"> <img width="1000" height="450" src="media/GA/ga61.jpg"> </p>

<div align="center"> 

**`Fig.91`Steps to re-assign record**

</div>

4.	Once the record is re-assigned, then the record status be changed to Reassigned record,

> Account Administrator able to re-assign records only in work assignments of task and Group administrator can only able to re-assign the data for work assignments of project Task.

### See on Map

First, select the records individually or multiple records and then click on �Inserted Locations�. All the submitted data be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga62.jpg"> </p>

<div align="center"> 

**`Fig.92`Steps to see insert location on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga63.jpg"> </p>

<div align="center"> 

**`Fig.93`Map shows inserted records**

</div>