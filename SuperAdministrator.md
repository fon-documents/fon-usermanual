﻿
# Super Administrator
<!-- {docsify-ignore} -->

Super administrator is single user who can view all accounts information. In Application we have Dashboard Icon on right side. It is common for all web users based on their roles all tasks, projects, licenses etc.  will be shown.

<p align="center"> <img width="1000" height="500" src="media/SUA.jpg"> </p>

<div align="center"> 

**`Fig` Super administrator activities**

</div>

<p>Enter valid Super administrator username and password and click on Sign in.</p>

Login as [Super administrator](https://demo.fieldon.com/#/login)

<p align="center"> <img width="1000" height="450" src="media/SU/SU1.jpg"> </p>

<div align="center"> 

**`Fig.1` FieldOn login page**

</div>

- On successful Login, Super administrator lands on Accounts page with Welcome message, list of below options in the menu bar:

 - `Accounts`	

 - `Administrators`

 - `Users`

 - `Categories`

 - `Device Management`

 - `Templates`

 - `Forms`

 - `Downloads`

 - `Settings`


<p align="center"> <img width="1000" height="450" src="media/SU/SU2.jpg"> </p>

<div align="center"> 

**`Fig.2` Accounts page**

</div>

<hr>











